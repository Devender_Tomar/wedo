package com.wedo;

import java.util.Arrays;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.wedo.dao.RoleDAO;
import com.wedo.helpers.HelperExtension;
import com.wedo.model.Role;

@SpringBootApplication
public class WedoApplication {

	HelperExtension helperExtension= new HelperExtension();
	public static void main(String[] args) {
		SpringApplication.run(WedoApplication.class, args);

	}

	 @Bean
     CommandLineRunner init (RoleDAO roleDao){
         return args -> {

        	 if(roleDao.findAll().size()==0)
        	 {
             List<String> names = Arrays.asList("Admin", "Hr","Employee","client");
             for(String name:names)
             {
            	 Role role=new Role();
            	 role.setRoleId("role"+helperExtension.getUniqueId());
            	 role.setRoleType(name);
            	 role.setCreatedOn(helperExtension.getDateTime());
            	 role.setUpdatedOn(helperExtension.getDateTime());
            	 roleDao.save(role);
             }
        	 }
         };
     }

}
