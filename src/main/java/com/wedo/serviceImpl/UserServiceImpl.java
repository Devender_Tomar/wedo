package com.wedo.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dao.RoleDAO;
import com.wedo.dao.UserDAO;
import com.wedo.dto.UserDTO;
import com.wedo.helpers.ConstantExtension;
import com.wedo.helpers.HelperExtension;
import com.wedo.model.Role;
import com.wedo.model.User;
import com.wedo.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDao;

	@Autowired
	private RoleDAO roleDao;

	ResponseModelList<UserDTO> responseModel = new ResponseModelList<>();
	HelperExtension helperExtension = new HelperExtension();

	private boolean status = false;
	private String message = "";
	private List<UserDTO> list = null;

	@Override
	public ResponseModelList<UserDTO> save(UserDTO userDTO) {
		list = new ArrayList<>();
		try {

			log.info("Saving User Information");
			User user = userDao.findByUsername(userDTO.getUsername());
			if (user != null) {
				return new ResponseModelList<UserDTO>(status, ConstantExtension.USER_NAME_ALREADY_EXIST, null);
			}

			List<User> listuser = userDao.findByEmail(userDTO.getEmail());
			if (listuser.size() > 0) {
				return new ResponseModelList<UserDTO>(status, ConstantExtension.USER_EMAIL_ALREADY_EXIST, null);
			}

			if (!userDTO.getContactno1().isEmpty())
				if (userDao.findByContactno1(userDTO.getContactno1()).size() > 0) {
					return new ResponseModelList<UserDTO>(status, ConstantExtension.USER_PHONE_NO1, null);
				}
			if (!userDTO.getContactno2().isEmpty())
				if (userDao.findByContactno2(userDTO.getContactno2()).size() > 0) {
					return new ResponseModelList<UserDTO>(status, ConstantExtension.USER_PHONE_NO2, null);
				}

			if (!helperExtension.isNullOrEmpty(userDTO.getUserid())) {
				putValueInResponseModel(true, ConstantExtension.USER_UPDATED, userDTO);
			} else {
				putValueInResponseModel(true, ConstantExtension.USER_ADDED, userDTO);
			}

			responseModel = new ResponseModelList<>(status, message, list);
		} catch (Exception e) {
			log.error("Error while saving user information in Service");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	@Override
	public ResponseModelList<UserDTO> findById(String userId) {
		list = new ArrayList<>();

		try {
			log.info("Finding User By Id");
			Optional<User> user = userDao.findById(userId);
			if (user.isPresent()) {
				UserDTO userDTO = new UserDTO();
				User user2 = user.get();
				Role role = roleDao.findById(user2.getRole().getRoleId()).get();
				user2.setRole(role);
				BeanUtils.copyProperties(user.get(), userDTO);
				list.add(userDTO);
			}
			log.info("Successfully Found User By Id");
			responseModel = new ResponseModelList<>(true, ConstantExtension.USER_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			log.error("Error while Finding User By Id");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	@Override
	public ResponseModelList<UserDTO> deleteById(String userId) {
		try {
			log.info("Deleting User By Id");
			Optional<User> user = userDao.findById(userId);
			if (user.isPresent()) {
				User user2 = user.get();
				user2.setIsFlag(0);
				user2 = userDao.save(user2);
				UserDTO userDto = new UserDTO();
				BeanUtils.copyProperties(user2, userDto);
				list.add(userDto);
			}
			log.info("Successfully Deleted User");
			responseModel = new ResponseModelList<>(true, ConstantExtension.USER_DELETED, list);
		} catch (Exception e) {
			log.error("Error while Deleting User By Id");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;
	}

	@Override
	public ResponseModelList<UserDTO> findAll(String userId) {
		list = new ArrayList<UserDTO>();
		try {
			log.info("Find All User");
			List<User> users = userDao.findAll();

			BeanUtils.copyProperties(users, list);

			log.info("SuccessFully Fetched All User");
			responseModel = new ResponseModelList<>(true, ConstantExtension.USER_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	@Override
	public ResponseModelList<UserDTO> deleteAll() {
		list = new ArrayList<UserDTO>();
		try {
			log.info("Deleting All User");
			List<User> users = userDao.findAll();
			for (User user : users) {
				user.setIsFlag(0);
				user = userDao.save(user);
				UserDTO userDTO = new UserDTO();
				BeanUtils.copyProperties(user, userDTO);
				list.add(userDTO);
			}

			log.info("SuccessFully Deleted All User");
			responseModel = new ResponseModelList<>(true, ConstantExtension.USER_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;

	}

	@Override
	public ResponseModelList<UserDTO> findByUserName(String username) {
		list = new ArrayList<UserDTO>();
		try {
			log.info("Fetching User by Username");
			User user = userDao.findByUsername(username);
			if (!helperExtension.isNullOrEmpty(user)) {
				UserDTO userDTO = new UserDTO();
				BeanUtils.copyProperties(user, userDTO);
				list.add(userDTO);
			}
			log.info("SuccessFully Fetched User by Username");
			responseModel = new ResponseModelList<>(true, ConstantExtension.USER_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			log.error("Error while fetching User by Username");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;

	}

	@Override
	public ResponseModelList<UserDTO> findByEmail(String email) {
		list = new ArrayList<UserDTO>();
		try {
			log.info("Fetching User by Username");
			User user = userDao.findByEmail(email).get(0);
			if (!helperExtension.isNullOrEmpty(user)) {
				UserDTO userDTO = new UserDTO();
				BeanUtils.copyProperties(user, userDTO);
				list.add(userDTO);
			}
			log.info("SuccessFully Fetched User by Email");
			responseModel = new ResponseModelList<>(true, ConstantExtension.USER_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			log.error("Error while fetching User by Email");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	@Override
	public ResponseModelList<UserDTO> findAllActive() {
		list = new ArrayList<>();
		try {

			log.info("fetching all  active User");
			List<User> users = userDao.findByIsFlag(1);
			for (User user : users) {
				user.setIsFlag(0);
				user = userDao.save(user);
				UserDTO userDTO = new UserDTO();
				BeanUtils.copyProperties(user, userDTO);
				list.add(userDTO);
			}

			log.info("SuccessFully Deleted All User");
			responseModel = new ResponseModelList<>(true, ConstantExtension.USER_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			log.error("Error while fetching all active user");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, list);
		}
		return responseModel;
	}

	private void putValueInResponseModel(boolean status, String message, UserDTO dto) {
		this.status = status;
		this.message = message;
		Date dateTime = helperExtension.getDateTime();
		Role role = new Role();
		if (!helperExtension.isNullOrEmpty(dto)) {
			User model = new User();
			BeanUtils.copyProperties(dto, model);

			if (!helperExtension.isNullOrEmpty(dto.getUserid())) {
				model.setUserid(dto.getUserid());
				model.setUpdatedOn(dateTime);
			} else {
				model.setUserid("user" + helperExtension.getUniqueId());
				role = roleDao.findById(dto.getRole().getRoleId()).get();
				model.setRole(role);
				model.setCreatedOn(dateTime);
			}
			model = userDao.save(model);
			BeanUtils.copyProperties(model, dto);
			BeanUtils.copyProperties(model.getRole(), dto.getRole());
			list.add(dto);
		}
	}

}
