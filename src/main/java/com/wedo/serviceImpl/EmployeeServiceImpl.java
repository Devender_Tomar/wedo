package com.wedo.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dao.AddressDao;
import com.wedo.dao.DepartmentDAO;
import com.wedo.dao.DesignationDAO;
import com.wedo.dao.EmployeeDAO;
import com.wedo.dto.EmployeeDTO;
import com.wedo.helpers.ConstantExtension;
import com.wedo.helpers.HelperExtension;
import com.wedo.model.Address;
import com.wedo.model.Department;
import com.wedo.model.Designation;
import com.wedo.model.Employee;
import com.wedo.service.EmployeeService;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeDAO dao;

	@Autowired
	DepartmentDAO departmentdao;

	@Autowired
	DesignationDAO designationdao;

	@Autowired
	AddressDao addressDao;

	public ResponseModelList<EmployeeDTO> responseModel = new ResponseModelList<EmployeeDTO>();
	public HelperExtension helperExtension = new HelperExtension();

	private boolean status = false;
	private String message = "";
	private List<EmployeeDTO> list = null;

	@Override
	public ResponseModelList<EmployeeDTO> save(EmployeeDTO employeeDTO) {

		try {
			list = new ArrayList<>();
			log.info("Saving Employee information");

			if (!helperExtension.isNullOrEmpty(employeeDTO.getEmployeeId())) {
				putValueInResponseModel(true, ConstantExtension.EMPLOYEE_UPDATED, employeeDTO);
			} else {
				putValueInResponseModel(true, ConstantExtension.EMPLOYEE_ADDED, employeeDTO);
			}
			log.info(message);
			responseModel = new ResponseModelList<>(status, message, list);
		} catch (Exception e) {
			log.error("error while saving Client information");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;
	}

	@Override
	public ResponseModelList<EmployeeDTO> findById(String employeeId) {

		try {
			log.info("Finding Employee by Id");
			list = new ArrayList<EmployeeDTO>();
			Employee employee = dao.findById(employeeId).get();
			if (employee != null) {
				EmployeeDTO employeeDTO = new EmployeeDTO();
				BeanUtils.copyProperties(employee, employeeDTO);
				BeanUtils.copyProperties(employee.getAddress(), employeeDTO.getAddress());
				BeanUtils.copyProperties(employee.getDepartment(), employeeDTO.getDepartment());
				BeanUtils.copyProperties(employee.getDesignation(), employeeDTO.getDesignation());
				list.add(employeeDTO);
				responseModel = new ResponseModelList<>(true, ConstantExtension.EMPLOYEE_SUCCESS_RECEIVE, list);
			} else {
				responseModel = new ResponseModelList<>(false, ConstantExtension.EMPLOYEE_NOT_FOUND, null);
			}

		} catch (Exception e) {
			log.error("Error while finding employee by id");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;
	}

	@Override
	public ResponseModelList<EmployeeDTO> deleteById(String employeeId) {
		try {
			log.info("Deleting Employee by Id");
			list = new ArrayList<EmployeeDTO>();
			Employee employee = dao.findById(employeeId).get();
			if (employee != null) {
				EmployeeDTO employeeDTO = new EmployeeDTO();
				employee.setIsFlag(0);
				employee = dao.save(employee);
				BeanUtils.copyProperties(employee, employeeDTO);
				list.add(employeeDTO);
				responseModel = new ResponseModelList<>(true, ConstantExtension.EMPLOYEE_SUCCESS_RECEIVE, list);
			} else {
				responseModel = new ResponseModelList<>(false, ConstantExtension.EMPLOYEE_NOT_FOUND, null);
			}

		} catch (Exception e) {
			log.error("Error while deleting employee by id");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;
	}

	@Override
	public ResponseModelList<EmployeeDTO> findByEmail(String email) {
		try {
			log.info("Finding Employee by Email");
			list = new ArrayList<EmployeeDTO>();
			Employee employee = dao.findByEmail(email);
			if (employee != null) {
				EmployeeDTO employeeDTO = new EmployeeDTO();
				BeanUtils.copyProperties(employee, employeeDTO);
				BeanUtils.copyProperties(employee.getAddress(), employeeDTO.getAddress());
				BeanUtils.copyProperties(employee.getDepartment(), employeeDTO.getDepartment());
				BeanUtils.copyProperties(employee.getDesignation(), employeeDTO.getDesignation());
				list.add(employeeDTO);
				responseModel = new ResponseModelList<>(true, ConstantExtension.EMPLOYEE_SUCCESS_RECEIVE, list);
			} else {
				responseModel = new ResponseModelList<>(false, ConstantExtension.EMPLOYEE_NOT_FOUND, null);
			}

		} catch (Exception e) {
			log.error("Error while finding employee by email");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;
	}

	@Override
	public ResponseModelList<EmployeeDTO> findByContact(String contact) {
		try {
			log.info("Fetching Client by Contact no");
			list = new ArrayList<EmployeeDTO>();
			boolean flag = false;
			Employee employee = dao.findByContactno1(contact);
			if (employee != null) {
				EmployeeDTO employeeDTO = new EmployeeDTO();
				BeanUtils.copyProperties(employee, employeeDTO);
				BeanUtils.copyProperties(employee.getAddress(), employeeDTO.getAddress());
				BeanUtils.copyProperties(employee.getDepartment(), employeeDTO.getDepartment());
				BeanUtils.copyProperties(employee.getDesignation(), employeeDTO.getDesignation());
				list.add(employeeDTO);
				flag = true;
			}
			employee = dao.findByContactno2(contact);
			if (employee != null && flag == false) {
				EmployeeDTO employeeDTO = new EmployeeDTO();
				BeanUtils.copyProperties(employee, employeeDTO);
				BeanUtils.copyProperties(employee.getAddress(), employeeDTO.getAddress());
				BeanUtils.copyProperties(employee.getDepartment(), employeeDTO.getDepartment());
				BeanUtils.copyProperties(employee.getDesignation(), employeeDTO.getDesignation());
				list.add(employeeDTO);
			}
			if (employee != null) {
				log.info(ConstantExtension.EMPLOYEE_SUCCESS_RECEIVE);
				responseModel = new ResponseModelList<>(true, ConstantExtension.EMPLOYEE_SUCCESS_RECEIVE, list);
			} else {
				log.info(ConstantExtension.CLIENT_NOT_FOUND);
				responseModel = new ResponseModelList<>(false, ConstantExtension.EMPLOYEE_NOT_FOUND, null);
			}

		} catch (Exception e) {
			log.error("Error while fetching employee by contact no.");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	@Override
	public ResponseModelList<EmployeeDTO> findByAadhar(String aadhar) {
		try {
			log.info("Finding Employee by Aadhar");
			list = new ArrayList<EmployeeDTO>();
			Employee employee = dao.findByAadharno(aadhar);
			if (employee != null) {
				EmployeeDTO employeeDTO = new EmployeeDTO();
				BeanUtils.copyProperties(employee, employeeDTO);
				BeanUtils.copyProperties(employee.getAddress(), employeeDTO.getAddress());
				BeanUtils.copyProperties(employee.getDepartment(), employeeDTO.getDepartment());
				BeanUtils.copyProperties(employee.getDesignation(), employeeDTO.getDesignation());
				list.add(employeeDTO);
				responseModel = new ResponseModelList<>(true, ConstantExtension.EMPLOYEE_SUCCESS_RECEIVE, list);
			} else {
				responseModel = new ResponseModelList<>(false, ConstantExtension.EMPLOYEE_NOT_FOUND, null);
			}
		} catch (Exception e) {
			log.error("Error while finding employee by aadhar");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;
	}

	@Override
	public ResponseModelList<EmployeeDTO> findByPanCard(String pancardno) {
		try {
			log.info("Finding Employee by PanCard");
			list = new ArrayList<EmployeeDTO>();
			Employee employee = dao.findByPancardno(pancardno);
			if (employee != null) {
				EmployeeDTO employeeDTO = new EmployeeDTO();
				BeanUtils.copyProperties(employee, employeeDTO);
				BeanUtils.copyProperties(employee.getAddress(), employeeDTO.getAddress());
				BeanUtils.copyProperties(employee.getDepartment(), employeeDTO.getDepartment());
				BeanUtils.copyProperties(employee.getDesignation(), employeeDTO.getDesignation());
				list.add(employeeDTO);
				responseModel = new ResponseModelList<>(true, ConstantExtension.EMPLOYEE_SUCCESS_RECEIVE, list);
			} else {
				responseModel = new ResponseModelList<>(false, ConstantExtension.EMPLOYEE_NOT_FOUND, null);
			}
		} catch (Exception e) {
			log.error("Error while finding employee by pancard");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;

	}

	@Override
	public ResponseModelList<EmployeeDTO> findByDepartment(String departmentId) {
		try {
			log.info("Finding Employee by department");
			list = new ArrayList<EmployeeDTO>();
			List<Employee> employees = dao.findByDepartment(departmentId);
			for (Employee employee : employees) {
				EmployeeDTO employeeDTO = new EmployeeDTO();
				BeanUtils.copyProperties(employee, employeeDTO);
				BeanUtils.copyProperties(employee.getAddress(), employeeDTO.getAddress());
				BeanUtils.copyProperties(employee.getDepartment(), employeeDTO.getDepartment());
				BeanUtils.copyProperties(employee.getDesignation(), employeeDTO.getDesignation());
				list.add(employeeDTO);
			}
			responseModel = new ResponseModelList<>(true, ConstantExtension.CLIENT_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			log.error("Error while finding employee by department");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;
	}

	@Override
	public ResponseModelList<EmployeeDTO> findByDesignation(String designationId) {
		try {
			log.info("Finding Employee by Designation");
			list = new ArrayList<EmployeeDTO>();
			List<Employee> employees = dao.findByDesignation(designationId);
			for (Employee employee : employees) {
				EmployeeDTO employeeDTO = new EmployeeDTO();
				BeanUtils.copyProperties(employee, employeeDTO);
				BeanUtils.copyProperties(employee.getAddress(), employeeDTO.getAddress());
				BeanUtils.copyProperties(employee.getDepartment(), employeeDTO.getDepartment());
				BeanUtils.copyProperties(employee.getDesignation(), employeeDTO.getDesignation());
				list.add(employeeDTO);
			}
			responseModel = new ResponseModelList<>(true, ConstantExtension.CLIENT_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			log.error("Error while finding employee by designation");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;
	}

	@Override
	public ResponseModelList<EmployeeDTO> findByUserType(String userType) {
		try {
			log.info("Finding employee by userType");
			list = new ArrayList<EmployeeDTO>();
			List<Employee> employees = dao.findByUserType(userType);
			for (Employee employee : employees) {
				EmployeeDTO employeeDTO = new EmployeeDTO();
				BeanUtils.copyProperties(employee, employeeDTO);
				BeanUtils.copyProperties(employee.getAddress(), employeeDTO.getAddress());
				BeanUtils.copyProperties(employee.getDepartment(), employeeDTO.getDepartment());
				BeanUtils.copyProperties(employee.getDesignation(), employeeDTO.getDesignation());
				list.add(employeeDTO);
			}
			responseModel = new ResponseModelList<>(true, ConstantExtension.CLIENT_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			log.error("Error while finding employee by UserType");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;
	}

	@Override
	public ResponseModelList<EmployeeDTO> findAll() {
		try {
			log.info("Finding all Employee");
			list = new ArrayList<EmployeeDTO>();
			List<Employee> employees = dao.findAll();
			for (Employee employee : employees) {
				EmployeeDTO employeeDTO = new EmployeeDTO();
				BeanUtils.copyProperties(employee, employeeDTO);
				if (employee.getDepartment() != null)
					BeanUtils.copyProperties(employee.getDepartment(), employeeDTO.getDepartment());
				if (employee.getDesignation() != null)
					BeanUtils.copyProperties(employee.getDesignation(), employeeDTO.getDesignation());
				if (employee.getAddress() != null)
					BeanUtils.copyProperties(employee.getAddress(), employeeDTO.getAddress());

				list.add(employeeDTO);
			}
			responseModel = new ResponseModelList<>(true, ConstantExtension.CLIENT_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			log.error("Error while finding all employee");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;
	}

	@Override
	public ResponseModelList<EmployeeDTO> deleteAll() {
		try {
			log.info("Deleting all employee");
			list = new ArrayList<EmployeeDTO>();
			List<Employee> employees = dao.findAll();
			for (Employee employee : employees) {
				EmployeeDTO employeeDTO = new EmployeeDTO();
				employee.setIsFlag(0);
				employee = dao.save(employee);
				BeanUtils.copyProperties(employee, employeeDTO);
				list.add(employeeDTO);
			}
			responseModel = new ResponseModelList<>(true, ConstantExtension.CLIENT_DELETED, list);
		} catch (Exception e) {
			log.error("Error while deleting all employee");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;
	}

	private void putValueInResponseModel(boolean status, String message, EmployeeDTO dto) {
		this.status = status;
		this.message = message;
		Date dateTime = helperExtension.getDateTime();
		if (!helperExtension.isNullOrEmpty(dto)) {
			Employee model = new Employee();
			BeanUtils.copyProperties(dto, model);
			if (!helperExtension.isNullOrEmpty(dto.getEmployeeId())) {
				model.setEmployeeId(dto.getEmployeeId());
				model.setUpdatedOn(dateTime);
			} else {
				model.setEmployeeId("emp" + helperExtension.getUniqueId());
				model.setCreatedOn(dateTime);
				model.setUpdatedOn(dateTime);
			}

			if (!dto.getDepartment().getDepartmentId().isEmpty()) {
				Department department = departmentdao.findById(dto.getDepartment().getDepartmentId()).get();
				model.setDepartment(department);
			}
			if (!dto.getDesignation().getDesignationId().isEmpty()) {
				Designation designation = designationdao.findById(dto.getDesignation().getDesignationId()).get();
				model.setDesignation(designation);
			}
			if (!StringUtils.isEmpty(dto.getAddress().getAddressid())) {
				Address address = addressDao.findById(dto.getAddress().getAddressid()).get();
				model.setAddress(address);
			} else {
				Address address = new Address();
				BeanUtils.copyProperties(dto.getAddress(), address);
				address.setAddressid("add" + helperExtension.getUniqueId());
				address = addressDao.save(address);
				model.setAddress(address);
			}
			model=dao.save(model);
			BeanUtils.copyProperties(model, dto);
			BeanUtils.copyProperties(model.getDesignation(), dto.getDesignation());
			BeanUtils.copyProperties(model.getDepartment(), dto.getDepartment());
			BeanUtils.copyProperties(model.getAddress(), dto.getAddress());
			list.add(dto);
		}
	}

}
