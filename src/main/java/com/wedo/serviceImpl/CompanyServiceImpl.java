package com.wedo.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dao.CompanyDAO;
import com.wedo.dto.CompanyDTO;
import com.wedo.helpers.ConstantExtension;
import com.wedo.helpers.HelperExtension;
import com.wedo.model.Company;
import com.wedo.service.CompanyService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CompanyServiceImpl implements CompanyService {

	@Autowired
	CompanyDAO companyDao;

	ResponseModelList<CompanyDTO> responseModel = new ResponseModelList<CompanyDTO>();
	HelperExtension helperExtension = new HelperExtension();

	private boolean status = false;
	private String message = "";
	private List<CompanyDTO> list = null;

	@Override
	public ResponseModelList<CompanyDTO> save(CompanyDTO companyDTO) {
		try {
			list = new ArrayList<>();
			log.info("Saving Company information");

			if (!helperExtension.isNullOrEmpty(companyDTO.getCompanyId())) {
				putValueInResponseModel(true, ConstantExtension.COMPANY_UPDATED, companyDTO);
			} else {
				putValueInResponseModel(true, ConstantExtension.COMPANY_ADDED, companyDTO);
			}
			responseModel = new ResponseModelList<>(status, message, list);
		} catch (Exception e) {
			log.error("error while saving company information");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;

	}

	@Override
	public ResponseModelList<CompanyDTO> findById(String companyId) {
		try {
			list=new ArrayList<CompanyDTO>();
			log.info("Finding company by id");
			Company company = companyDao.findById(companyId).get();
			if (company != null) {
				CompanyDTO companyDTO = new CompanyDTO();
				log.info(ConstantExtension.COMPANY_FETCHED);
				BeanUtils.copyProperties(company, companyDTO);
				list.add(companyDTO);
				responseModel = new ResponseModelList<>(true, ConstantExtension.COMPANY_FETCHED, list);

			} else {
				log.info(ConstantExtension.COMPANY_ENTRIES);
				responseModel = new ResponseModelList<>(true, ConstantExtension.COMPANY_ENTRIES, list);
			}
		} catch (Exception e) {
			log.error("Error while finding company record");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	@Override
	public ResponseModelList<CompanyDTO> deleteById(String companyId) {
		try {
			list=new ArrayList<CompanyDTO>();
			log.info("deleting company by id");
			Company company = companyDao.findById(companyId).get();
			if (company != null) {
				CompanyDTO companyDTO = new CompanyDTO();
				log.info(ConstantExtension.COMPANY_DELETED);
				company.setIsFlag(0);
				company = companyDao.save(company);
				BeanUtils.copyProperties(company, companyDTO);
				list.add(companyDTO);
				responseModel = new ResponseModelList<>(true, ConstantExtension.COMPANY_DELETED, list);

			} else {
				log.info(ConstantExtension.COMPANY_ENTRIES);
				responseModel = new ResponseModelList<>(true, ConstantExtension.COMPANY_ENTRIES, list);
			}
		} catch (Exception e) {
			log.error("Error while finding company record");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;

	}

	@Override
	public ResponseModelList<CompanyDTO> findAll() {
		try {
			list=new ArrayList<CompanyDTO>();
			log.info("Finding company by id");
			List<Company> companies = companyDao.findAll();
			if (companies.size() > 0) {
				for (Company company : companies) {
					CompanyDTO companyDTO = new CompanyDTO();
					log.info(ConstantExtension.COMPANY_FETCHED);
					BeanUtils.copyProperties(company, companyDTO);
					list.add(companyDTO);
				}
				responseModel = new ResponseModelList<>(true, ConstantExtension.COMPANY_FETCHED, list);

			} else {
				log.info(ConstantExtension.COMPANY_ENTRIES);
				responseModel = new ResponseModelList<>(true, ConstantExtension.COMPANY_ENTRIES, list);
			}
		} catch (Exception e) {
			log.error("Error while finding company record");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;

	}

	private void putValueInResponseModel(boolean status, String message, CompanyDTO dto) {
		this.status = status;
		this.message = message;
		Date dateTime = helperExtension.getDateTime();
		if (!helperExtension.isNullOrEmpty(dto)) {
			Company model = new Company();
			BeanUtils.copyProperties(dto, model);
			if (!helperExtension.isNullOrEmpty(dto.getCompanyId())) {
				model.setCompanyId(dto.getCompanyId());
				model.setUpdatedOn(dateTime);
			} else {
				model.setCompanyId("com" + helperExtension.getUniqueId());
				model.setCreatedOn(dateTime);
				model.setUpdatedOn(dateTime);
			}
			model = companyDao.save(model);
			BeanUtils.copyProperties(model, dto);
			list.add(dto);
		}
	}

}
