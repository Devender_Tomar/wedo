package com.wedo.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dao.ClientDAO;
import com.wedo.dao.FeedbackDAO;
import com.wedo.dto.FeedbackDTO;
import com.wedo.helpers.ConstantExtension;
import com.wedo.helpers.HelperExtension;
import com.wedo.model.Client;
import com.wedo.model.Feedback;
import com.wedo.service.FeedbackService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class FeedbackServiceImpl implements FeedbackService {

	@Autowired
	FeedbackDAO dao;


	@Autowired
	ClientDAO clientDAO;

	ResponseModelList<FeedbackDTO> responseModel = new ResponseModelList<FeedbackDTO>();
	HelperExtension helperExtension = new HelperExtension();

	private boolean status = false;
	private String message = "";
	private List<FeedbackDTO> list = null;

	@Override
	public ResponseModelList<FeedbackDTO> save(FeedbackDTO feedbackDTO) {
		try {
			list = new ArrayList<>();
			log.info("Saving Feedback information");

			if (!helperExtension.isNullOrEmpty(feedbackDTO.getFeedbackId())) {
				putValueInResponseModel(true, ConstantExtension.FEEDBACK_UPDATED, feedbackDTO);
			} else {
				putValueInResponseModel(true, ConstantExtension.FEEDBACK_ADDED, feedbackDTO);
			}
			responseModel = new ResponseModelList<>(status, message, list);
		} catch (Exception e) {
			log.error("error while saving Feedback information");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;
	}

	@Override
	public ResponseModelList<FeedbackDTO> findById(String feedbackId) {
		try {
			log.info("finding Feedback information");
			if (!helperExtension.isNullOrEmpty(feedbackId)) {
				boolean flag = dao.existsById(feedbackId);
				if (flag) {
					Optional<Feedback> feedback = dao.findById(feedbackId);
					Feedback feedback2 = feedback.get();
					FeedbackDTO feedbackDTO = new FeedbackDTO();
					BeanUtils.copyProperties(feedback2, feedbackDTO);
					list.add(feedbackDTO);
					responseModel = new ResponseModelList<>(true, ConstantExtension.FEEDBACK_ID_WRONG, list);
				} else {
					responseModel = new ResponseModelList<>(true, ConstantExtension.FEEDBACK_ID_WRONG, list);
				}
			}
		} catch (Exception e) {
			log.error("error while finding Feedback information");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;
	}


	@Override
	public ResponseModelList<FeedbackDTO> findAll() {
		try {
			log.info("finding all Feedback information");
			List<Feedback> feedback2 = dao.findAll();
			if (feedback2.size() > 0) {
				for (Feedback feedback : feedback2) {
					FeedbackDTO feedbackDTO = new FeedbackDTO();
					BeanUtils.copyProperties(feedback, feedbackDTO);
					list.add(feedbackDTO);
				}
				responseModel = new ResponseModelList<>(true, ConstantExtension.FEEDBACK_FETCHED, list);
			} else {
				responseModel = new ResponseModelList<>(true, ConstantExtension.FEEDBACK_ENTRIES, list);
			}

		} catch (Exception e) {
			log.error("error while finding all Feedback information");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	@Override
	public ResponseModelList<FeedbackDTO> deleteById(String feedbackId) {
		try {
			log.info("deleting Feedback information  by id");
			if (!helperExtension.isNullOrEmpty(feedbackId)) {
				boolean flag = dao.existsById(feedbackId);
				if (flag) {
					Optional<Feedback> feedback = dao.findById(feedbackId);
					Feedback feedback2 = feedback.get();
					feedback2.setIsFlag(0);
					feedback2 = dao.save(feedback2);
					FeedbackDTO feedbackDTO = new FeedbackDTO();
					BeanUtils.copyProperties(feedback2, feedbackDTO);
					list.add(feedbackDTO);
					responseModel = new ResponseModelList<>(false, ConstantExtension.FEEDBACK_DELETED, list);
				} else {
					responseModel = new ResponseModelList<>(false, ConstantExtension.FEEDBACK_ID_WRONG, null);
				}
				log.info("Successfully deleted Feedback information by id");
			}
		} catch (Exception e) {
			log.error("error while deleting Feedback information by id");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;
	}

	@Override
	public ResponseModelList<FeedbackDTO> deleteAll() {
		try {
			log.info("Saving Feedback information");
			list = new ArrayList<FeedbackDTO>();
			List<Feedback> feedbacks = dao.findAll();
			if (feedbacks.size() > 0) {
				for (Feedback feedback : feedbacks) {
					FeedbackDTO dto = new FeedbackDTO();
					feedback.setIsFlag(0);
					feedback =dao.save(feedback);
					BeanUtils.copyProperties(feedback, dto);
					list.add(dto);
				}
				log.info("Successfully Fetched");
				responseModel = new ResponseModelList<>(true, ConstantExtension.FEEDBACK_FETCHED, list);
			} else {
				responseModel = new ResponseModelList<>(true, ConstantExtension.FEEDBACK_ENTRIES, list);
			}

		} catch (Exception e) {
			log.error("error while saving Feedback information");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;
	}

	@Override
	public ResponseModelList<FeedbackDTO> findByIdClientId(String clientId) {
		try {
			log.info("Saving Feedback information");
			list = new ArrayList<FeedbackDTO>();
			if (!helperExtension.isNullOrEmpty(clientId)) {
				List<Feedback> feedbacks = dao.findbyClientId(clientId);
				if (feedbacks.size() > 0) {
					for (Feedback feedback : feedbacks) {
						FeedbackDTO feedbackDTO = new FeedbackDTO();
						BeanUtils.copyProperties(feedback, feedbackDTO);
						list.add(feedbackDTO);
					}
				} else
					responseModel = new ResponseModelList<>(true, ConstantExtension.FEEDBACK_ENTRIES, list);

			} else
				responseModel = new ResponseModelList<>(true, ConstantExtension.FEEDBACK_CLIENT, list);

		} catch (Exception e) {
			log.error("error while saving Feedback information");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;
	}

	private void putValueInResponseModel(boolean status, String message, FeedbackDTO dto) {
		this.status = status;
		this.message = message;
		Date dateTime = helperExtension.getDateTime();
		if (!helperExtension.isNullOrEmpty(dto)) {
			Feedback model = new Feedback();
			BeanUtils.copyProperties(dto, model);
			if (!helperExtension.isNullOrEmpty(dto.getFeedbackId())) {

				model.setFeedbackId(dto.getFeedbackId());

				model.setUpdatedOn(dateTime);
			} else {
				model.setFeedbackId("feed" + helperExtension.getUniqueId());
				Client client=clientDAO.findById(dto.getClient().getClientId()).get();
				model.setCreatedOn(dateTime);
				model.setClient(client);
			}
			model = dao.save(model);
			BeanUtils.copyProperties(model, dto);
			list.add(dto);
		}
	}
}
