package com.wedo.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dao.ServiceDAO;
import com.wedo.dto.ServiceDTO;
import com.wedo.helpers.ConstantExtension;
import com.wedo.helpers.HelperExtension;
import com.wedo.service.ServiceService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ServiceServiceImpl implements ServiceService {

	@Autowired
	ServiceDAO serviceDao;

	ResponseModelList<ServiceDTO> responseModel = new ResponseModelList<>();
	HelperExtension helperExtension = new HelperExtension();

	private boolean status = false;
	private String message = "";
	private List<ServiceDTO> list = null;

	@Override
	public ResponseModelList<ServiceDTO> save(ServiceDTO serviceDTO) {
		list = new ArrayList<>();
		try {
			log.info("Saving User Information");

			serviceDTO.setServiceName(helperExtension.camelCase(serviceDTO.getServiceName().trim()));
			com.wedo.model.Service service = serviceDao.findByServiceName(serviceDTO.getServiceName());
			if (service!=null) {
				return new ResponseModelList<ServiceDTO>(false, ConstantExtension.SERVICE_ALREADY, null);
			}
			if (!helperExtension.isNullOrEmpty(serviceDTO.getServiceId())) {
				putValueInResponseModel(true, ConstantExtension.SERVICE_UPDATED, serviceDTO);
			} else {
				putValueInResponseModel(true, ConstantExtension.SERVICE_ADDED, serviceDTO);
			}
			log.info("Successfully saved Service information");
			responseModel = new ResponseModelList<>(status, message, list);
		} catch (Exception e) {
			log.error("Error while saving service information in Service");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	@Override
	public ResponseModelList<ServiceDTO> findById(String serviceId) {
		list = new ArrayList<>();
		try {
			log.info("Finding Service by Service Id");

			if (!helperExtension.isNullOrEmpty(serviceId)) {
				Optional<com.wedo.model.Service> service = serviceDao.findById(serviceId);

				if (helperExtension.isNullOrEmpty(service.get().getServiceId())) {
					return new ResponseModelList<>(false, ConstantExtension.SERVICE_WRONG, null);
				}
				ServiceDTO serviceDTO = new ServiceDTO();
				BeanUtils.copyProperties(service.get(), serviceDTO);
				list.add(serviceDTO);
			}
			responseModel = new ResponseModelList<>(true, ConstantExtension.SERVICE_FETCHED, list);
		} catch (Exception e) {
			log.info("Error while finding Service by Service Id");
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
			e.printStackTrace();

		}

		return responseModel;
	}

	@Override
	public ResponseModelList<ServiceDTO> deleteById(String serviceId) {
		list = new ArrayList<>();
		try {
			log.info("Deleting Service by Service Id");

			if (!helperExtension.isNullOrEmpty(serviceId)) {
				Optional<com.wedo.model.Service> service = serviceDao.findById(serviceId);

				if (helperExtension.isNullOrEmpty(service.get().getServiceId())) {
					return new ResponseModelList<>(false, ConstantExtension.SERVICE_WRONG, null);
				}
				ServiceDTO serviceDTO = new ServiceDTO();
				com.wedo.model.Service service2 = service.get();
				service2.setIsFlag(0);
				service2 = serviceDao.save(service2);
				BeanUtils.copyProperties(service2, serviceDTO);
				list.add(serviceDTO);
			}
			responseModel = new ResponseModelList<>(true, ConstantExtension.SERVICE_DELETED, list);
		} catch (Exception e) {
			log.info("Error while deleting Service by Service Id");
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
			e.printStackTrace();

		}

		return responseModel;
	}

	@Override
	public ResponseModelList<ServiceDTO> deleteAll() {
		list = new ArrayList<>();
		try {
			log.info("Deleting All Service");
			List<com.wedo.model.Service> service = serviceDao.findAll();
			for (com.wedo.model.Service service2 : service) {
				service2.setIsFlag(0);
				service2 = serviceDao.save(service2);
				ServiceDTO serviceDTO = new ServiceDTO();
				BeanUtils.copyProperties(service2, serviceDTO);
				list.add(serviceDTO);
			}

			responseModel = new ResponseModelList<>(true, ConstantExtension.SERVICE_DELETED, list);
		} catch (Exception e) {
			log.info("Error while Deleting All Service");
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
			e.printStackTrace();

		}

		return responseModel;

	}

	@Override
	public ResponseModelList<ServiceDTO> findAll() {
		list = new ArrayList<>();
		try {
			log.info("Finding All Service");
			List<com.wedo.model.Service> service = serviceDao.findAll();
			for (com.wedo.model.Service service2 : service) {
				ServiceDTO serviceDTO = new ServiceDTO();
				BeanUtils.copyProperties(service2, serviceDTO);
				list.add(serviceDTO);
			}

			responseModel = new ResponseModelList<>(true, ConstantExtension.SERVICE_FETCHED, list);
		} catch (Exception e) {
			log.info("Error while finding All Service");
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
			e.printStackTrace();

		}

		return responseModel;
	}

	private void putValueInResponseModel(boolean status, String message, ServiceDTO dto) {
		this.status = status;
		this.message = message;
		Date dateTime = helperExtension.getDateTime();
		if (!helperExtension.isNullOrEmpty(dto)) {
			com.wedo.model.Service model = new com.wedo.model.Service();
			BeanUtils.copyProperties(dto, model);

			if (!helperExtension.isNullOrEmpty(dto.getServiceId())) {
				model.setServiceId(dto.getServiceId());
				model.setUpdatedOn(dateTime);
			} else {
				model.setServiceId("ser" + helperExtension.getUniqueId());
				model.setCreatedOn(dateTime);
			}
			model=serviceDao.save(model);
			BeanUtils.copyProperties(model, dto);
			list.add(dto);
		}
	}

}


