package com.wedo.serviceImpl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dao.CompanyDAO;
import com.wedo.dao.MailAttachmentDAO;
import com.wedo.dao.MailDAO;
import com.wedo.dto.MailAttachmentsDTO;
import com.wedo.dto.MailingDTO;
import com.wedo.helpers.ConstantExtension;
import com.wedo.helpers.HelperExtension;
import com.wedo.model.Client;
import com.wedo.model.Company;
import com.wedo.model.Employee;
import com.wedo.model.MailAttachments;
import com.wedo.model.Mailing;
import com.wedo.service.MailService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MailingServiceImpl implements MailService {

	@Autowired
	CompanyDAO companyDao;

	private String path = "src\\main\\resources\\Mail_Doc\\";

	@Autowired
	MailDAO mailDAO;

	@Autowired
	MailAttachmentDAO attachmentDAO;

	List<MailAttachmentsDTO> attachmentsDTOs = null;

	ResponseModelList<MailingDTO> responseModel = new ResponseModelList<MailingDTO>();
	HelperExtension helperExtension = new HelperExtension();

	private List<String> location = null;

	private boolean status = false;
	private String message = "";
	private List<MailingDTO> list = null;

	@Override
	public ResponseModelList<MailingDTO> save(List<MultipartFile> file, MailingDTO mailingDTO) {
		try {
			location=new ArrayList<>();
			list = new ArrayList<>();
			log.info("Saving Client information");
			if (sendMailWithAttachment(file, mailingDTO)) {
					putValueInResponseModel(true, ConstantExtension.MAIL_SENT, mailingDTO);
				log.info(message);
				responseModel = new ResponseModelList<>(status, message, list);
			} else {
				responseModel = new ResponseModelList<>(false, ConstantExtension.MAIL_ERROR, null);
			}
		} catch (Exception e) {
			log.error("error while sending mail");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;
	}

	@Override
	public ResponseModelList<MailingDTO> findAll() {

		return responseModel;
	}

	@Override
	public ResponseModelList<MailingDTO> findById(String mailId) {

		return responseModel;
	}

	@Override
	public ResponseModelList<MailingDTO> deleteById(String mailId) {

		return responseModel;
	}

	@Override
	public ResponseModelList<MailingDTO> deleteAll() {

		return responseModel;
	}

	public boolean sendMailWithAttachment(List<MultipartFile> files, MailingDTO mailingDTOs) {

		boolean mailstatus = false;
		Company company = companyDao.findAll().get(0);
		String from = company.getCompanyemail();
		final String username = company.getCompanyemail();
		final String password = company.getCompanyemailpassword();

		System.out.println("TLSEmail Start");
		Properties properties = System.getProperties();

		properties.setProperty("mail.smtp.host", company.getMailserver());
		properties.put("mail.smtp.port", company.getMailsmtpport());
		properties.put("mail.smtp.auth", company.getMailsmtpauth());
		properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

		Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			if (mailingDTOs.getClients().size() > 0) {
				List<String> emails = mailingDTOs.getClients().stream().map(column -> column.getClientEmailId())
						.collect(Collectors.toList());
				for (String address : emails)
					if (!address.isEmpty())
						message.addRecipient(Message.RecipientType.TO, new InternetAddress(address));
			}
			if (mailingDTOs.getEmployees().size() > 0) {
				List<String> emails = mailingDTOs.getEmployees().stream().map(column -> column.getEmail())
						.collect(Collectors.toList());
				for (String address : emails)
					if (!address.isEmpty())
						message.addRecipient(Message.RecipientType.TO, new InternetAddress(address));
			}

			message.setSubject(mailingDTOs.getSubject());
			message.setText(mailingDTOs.getMessage());
			BodyPart messageTextPart = new MimeBodyPart();
			messageTextPart.setText(mailingDTOs.getMessage());

			List<BodyPart> messageAttachmentPart = new ArrayList<>();
			for (MultipartFile multipartFile : files) {
				BodyPart messageAttachment = new MimeBodyPart();
				File file = write(multipartFile);
				DataSource source = new FileDataSource(file);
				messageAttachment.setDataHandler(new DataHandler(source));
				messageAttachment.setFileName(file.getName());
				messageAttachmentPart.add(messageAttachment);

			}
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageTextPart);
			for (BodyPart bodyPart : messageAttachmentPart)
				multipart.addBodyPart(bodyPart);
			message.setContent(multipart);
			Transport.send(message);
			mailstatus = true;
			System.out.println("Yo it has been sent..");
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
		return mailstatus;
	}

	public File write(MultipartFile file) {
		File convFile = new File(path + file.getOriginalFilename());
		try {
			convFile.createNewFile();
			location.add(path + file.getOriginalFilename());
			FileOutputStream fos = new FileOutputStream(convFile);
			fos.write(file.getBytes());
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(convFile);
		return convFile;
	}

	private void putValueInResponseModel(boolean status, String message, MailingDTO dto) {

		attachmentsDTOs = new ArrayList<>();
		this.status = status;
		this.message = message;
		Date dateTime = helperExtension.getDateTime();
		if (!helperExtension.isNullOrEmpty(dto)) {
			Mailing model = new Mailing();
			dto.setStatus("true");
			BeanUtils.copyProperties(dto, model);
			if (dto.getEmployees().size() > 0 && dto.getEmployees() != null)
			{
				List<Employee> employe= new ArrayList<Employee>(0);
				BeanUtils.copyProperties(dto.getEmployees(), employe);
				model.setEmployees(employe);
			}
			if (dto.getClients().size() > 0 && dto.getClients() != null)
			{
				List<Client> clients= new ArrayList<Client>(0);
				BeanUtils.copyProperties(dto.getClients(), clients);
				model.setClients(clients);
			}
			if (!helperExtension.isNullOrEmpty(dto.getMailingId())) {
				model.setMailingId(dto.getMailingId());
				model.setUpdatedOn(dateTime);
			} else {
				model.setMailingId("mail" + helperExtension.getUniqueId());
				if(dto.getEmployees().size()>0&&dto.getEmployees()!=null)
				{
					List<Employee> employees= new  ArrayList<Employee>(0);
					BeanUtils.copyProperties(dto.getEmployees(), employees);
					model.setEmployees(employees);
				}
				if(dto.getClients().size()>0&&dto.getClients()!=null)
				{
					List<Client> clients=new ArrayList<Client>(0);
					BeanUtils.copyProperties(dto.getClients(), clients);
					model.setClients(clients);
				}

				model.setCreatedOn(dateTime);
				model.setUpdatedOn(dateTime);
			}
			model = mailDAO.save(model);
			for (String locate : location) {
				MailAttachmentsDTO mailAttachmentsDTO = new MailAttachmentsDTO();
				MailAttachments attachments = new MailAttachments();
				attachments.setAttachmentId("Att" + helperExtension.getUniqueId());
				attachments.setMailing(model);
				attachments.setLocation(locate);
				attachments = attachmentDAO.save(attachments);
				BeanUtils.copyProperties(attachments, mailAttachmentsDTO);
				attachmentsDTOs.add(mailAttachmentsDTO);
			}
			BeanUtils.copyProperties(model, dto);
			if (model.getEmployees().size() > 0 && model.getEmployees() != null)
			{
				List<Employee> employees=model.getEmployees();
				BeanUtils.copyProperties(employees, dto.getEmployees());

			}
			if (model.getClients().size() > 0 && model.getClients() != null)
			{
				List<Client> clients=model.getClients();
				BeanUtils.copyProperties(clients, dto.getClients());
			}
			if(attachmentsDTOs.size()>0)
			{
				dto.setAttachments(attachmentsDTOs);
			}
			list.add(dto);
		}
	}

}
