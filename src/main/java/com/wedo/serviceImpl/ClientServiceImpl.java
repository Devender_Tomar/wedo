package com.wedo.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dao.ClientDAO;
import com.wedo.dto.ClientDTO;
import com.wedo.helpers.ConstantExtension;
import com.wedo.helpers.HelperExtension;
import com.wedo.model.Client;
import com.wedo.service.ClientService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ClientServiceImpl implements ClientService {

	@Autowired
	ClientDAO dao;

	ResponseModelList<ClientDTO> responseModel = new ResponseModelList<ClientDTO>();
	HelperExtension helperExtension = new HelperExtension();

	private boolean status = false;
	private String message = "";
	private List<ClientDTO> list = null;

	@Override
	public ResponseModelList<ClientDTO> save(ClientDTO clientDTO) {
		try {
			list = new ArrayList<>();
			log.info("Saving Client information");

			if (!helperExtension.isNullOrEmpty(clientDTO.getClientId())) {
				putValueInResponseModel(true, ConstantExtension.CLIENT_UPDATED, clientDTO);
			} else {
				putValueInResponseModel(true, ConstantExtension.CLIENT_ADDED, clientDTO);
			}
			log.info(message);
			responseModel = new ResponseModelList<>(status, message, list);
		} catch (Exception e) {
			log.error("error while saving Client information");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;
	}

	@Override
	public ResponseModelList<ClientDTO> findById(String clientId) {
		list = new ArrayList<>();
		try {
			ClientDTO clientDTO = new ClientDTO();
			Client client = dao.findById(clientId).get();
			if (client != null) {
				BeanUtils.copyProperties(client, clientDTO);
			}
			list.add(clientDTO);
			log.info(ConstantExtension.CLIENT_SUCCESS_RECEIVE);
			responseModel = new ResponseModelList<>(true, ConstantExtension.CLIENT_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	@Override
	public ResponseModelList<ClientDTO> deleteById(String clientId) {
		list = new ArrayList<ClientDTO>();
		try {
			ClientDTO clientDTO = new ClientDTO();
			Client client = dao.findById(clientId).get();
			if (client != null) {
				client.setIsFlag(0);
				client = dao.save(client);
				BeanUtils.copyProperties(client, clientDTO);
			}
			list.add(clientDTO);
			log.info(ConstantExtension.CLIENT_DELETED);
			responseModel = new ResponseModelList<>(true, ConstantExtension.CLIENT_DELETED, list);
		} catch (Exception e) {
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	@Override
	public ResponseModelList<ClientDTO> findAll() {
		try {
			list = new ArrayList<>();
			log.info("Find All Client");
			List<Client> clients = dao.findAll();
			for (Client client : clients) {
				ClientDTO clientDTO = new ClientDTO();
				BeanUtils.copyProperties(client, clientDTO);
				list.add(clientDTO);
			}
			log.info(ConstantExtension.CLIENT_SUCCESS_RECEIVE);
			responseModel = new ResponseModelList<>(true, ConstantExtension.CLIENT_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			log.error("Error while fetching all client");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	@Override
	public ResponseModelList<ClientDTO> deleteAll() {
		try {
			log.info("Deleting All Clients");
			list = new ArrayList<ClientDTO>();
			List<Client> clients = dao.findAll();
			for (Client client : clients) {
				client.setIsFlag(0);
				client = dao.save(client);
				ClientDTO clientDTO = new ClientDTO();
				BeanUtils.copyProperties(client, clientDTO);
				list.add(clientDTO);
			}
			log.info(ConstantExtension.CLIENT_DELETED);
			responseModel = new ResponseModelList<>(true, ConstantExtension.CLIENT_DELETED, list);
		} catch (Exception e) {
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	@Override
	public ResponseModelList<ClientDTO> findByContact(String contactNo) {
		try {
			log.info("Fetching Client by Contact no");
			list = new ArrayList<ClientDTO>();
			boolean flag = false;
			Client client = dao.findByClientContactNo1(contactNo);
			if (client != null) {
				ClientDTO clientDTO = new ClientDTO();
				BeanUtils.copyProperties(client, clientDTO);
				list.add(clientDTO);
				flag = true;
			}
			client = dao.findByClientContactNo2(contactNo);
			if (client != null && flag == false) {
				ClientDTO clientDTO = new ClientDTO();
				BeanUtils.copyProperties(client, clientDTO);
				list.add(clientDTO);
			}
			if (client!=null) {
				log.info(ConstantExtension.CLIENT_SUCCESS_RECEIVE);
				responseModel = new ResponseModelList<>(true, ConstantExtension.CLIENT_SUCCESS_RECEIVE, list);
			} else {
				log.info(ConstantExtension.CLIENT_NOT_FOUND);
				responseModel = new ResponseModelList<>(false, ConstantExtension.CLIENT_PHONE_NOT_FOUND, null);
			}

		} catch (Exception e) {
			log.error("Error while fetching client by contact no.");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	@Override
	public ResponseModelList<ClientDTO> findByAadharNO(String aadharNo) {
		try {
			list = new ArrayList<>();
			log.info("Find Client by Aadhar no");
			Client client = dao.findByClientAadharCard(aadharNo);
			ClientDTO clientDTO = new ClientDTO();
			if (client != null) {
				BeanUtils.copyProperties(client, clientDTO);
				list.add(clientDTO);
			}
			log.info(ConstantExtension.CLIENT_SUCCESS_RECEIVE);
			responseModel = new ResponseModelList<>(true, ConstantExtension.CLIENT_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	@Override
	public ResponseModelList<ClientDTO> findByEmail(String email) {
		try {
			list = new ArrayList<ClientDTO>();
			log.info("Finding client by email");
			Client client = dao.findByClientEmailId(email);
			if (client != null) {
				ClientDTO clientDTO = new ClientDTO();
				BeanUtils.copyProperties(client, clientDTO);
				list.add(clientDTO);
			}
			log.info(ConstantExtension.CLIENT_SUCCESS_RECEIVE);
			responseModel = new ResponseModelList<>(true, ConstantExtension.CLIENT_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			log.error("Error while fetching Client by email");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	@Override
	public ResponseModelList<ClientDTO> findByName(String name) {
		try {
			list = new ArrayList<>();
			log.info("Find client by name");
			List<Client> client = dao.findByClientFirstName(name);
			for (Client client2 : client) {
				ClientDTO clientDTO = new ClientDTO();
				BeanUtils.copyProperties(client2, clientDTO);
				list.add(clientDTO);
			}
			log.info(ConstantExtension.CLIENT_SUCCESS_RECEIVE);
			responseModel = new ResponseModelList<>(true, ConstantExtension.CLIENT_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			log.error("Error while fetching client by name");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	@Override
	public ResponseModelList<ClientDTO> findAllActive() {
		try {
			list = new ArrayList<ClientDTO>();
			log.info("Finding All Active Client");
			List<Client> clients = dao.findByIsFlag(1);
			for (Client client : clients) {
				ClientDTO clientDTO = new ClientDTO();
				BeanUtils.copyProperties(client, clientDTO);
				list.add(clientDTO);
			}
			log.info(ConstantExtension.CLIENT_SUCCESS_RECEIVE);
			responseModel = new ResponseModelList<>(true, ConstantExtension.CLIENT_SUCCESS_RECEIVE, list);
		} catch (Exception e) {
			log.error("Error while fetching All Active Client Information");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	private void putValueInResponseModel(boolean status, String message, ClientDTO dto) {
		this.status = status;
		this.message = message;
		Date dateTime = helperExtension.getDateTime();
		if (!helperExtension.isNullOrEmpty(dto)) {
			Client model = new Client();
			BeanUtils.copyProperties(dto, model);
			if (!helperExtension.isNullOrEmpty(dto.getClientId())) {
				model.setClientId(dto.getClientId());
				model.setUpdatedOn(dateTime);
			} else {
				model.setClientId("cli" + helperExtension.getUniqueId());
				model.setCreatedOn(dateTime);
				model.setUpdatedOn(dateTime);
			}
			model = dao.save(model);
			BeanUtils.copyProperties(model, dto);
			list.add(dto);
		}
	}

}
