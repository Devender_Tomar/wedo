package com.wedo.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dao.DesignationDAO;
import com.wedo.dto.DesignationDTO;
import com.wedo.helpers.ConstantExtension;
import com.wedo.helpers.HelperExtension;
import com.wedo.model.Designation;
import com.wedo.service.DesignationService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DesignationServiceImpl implements DesignationService {



	@Autowired
	DesignationDAO dao;

	ResponseModelList<DesignationDTO> responseModel = new ResponseModelList<>();
	HelperExtension helperExtension = new HelperExtension();

	private boolean status = false;
	private String message = "";
	private List<DesignationDTO> list = null;

	@Override
	public ResponseModelList<DesignationDTO> save(DesignationDTO designationDTO) {
		try {
			list = new ArrayList<>();
			log.info("Saving Designation information");

			if (!helperExtension.isNullOrEmpty(designationDTO.getDesignationId())) {
				putValueInResponseModel(true, ConstantExtension.DESIGNATION_UPDATED, designationDTO);
			} else {
				putValueInResponseModel(true, ConstantExtension.DESIGNATION_ADDED, designationDTO);
			}
			log.info(message);
			responseModel = new ResponseModelList<>(status, message, list);
		} catch (Exception e) {
			log.error("error while saving designation information");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;

	}

	@Override
	public ResponseModelList<DesignationDTO> findById(String designationId) {
		try {
			log.info("Finding designation by id");
			list = new ArrayList<DesignationDTO>();
			Designation designation = dao.findById(designationId).get();
			if (designation != null) {
				DesignationDTO designationDTO = new DesignationDTO();
				BeanUtils.copyProperties(designation, designationDTO);
				list.add(designationDTO);
				log.info(ConstantExtension.DESIGNATION_SUCCESS_RECEIVE);
				responseModel = new ResponseModelList<>(true, ConstantExtension.DESIGNATION_SUCCESS_RECEIVE, list);
			} else {
				log.info(ConstantExtension.DESIGNATION_NOT_FOUND);
				responseModel = new ResponseModelList<>(true, ConstantExtension.DESIGNATION_NOT_FOUND, list);
			}
		} catch (Exception e) {
			log.error("Error while finding designation by id");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	/* Pending here */

	@Override
	public ResponseModelList<DesignationDTO> deleteById(String designationId) {
		try {
			log.info("Deleting designation by id");
			list = new ArrayList<DesignationDTO>();
			Designation designation = dao.findById(designationId).get();
			if (designation != null) {
				DesignationDTO designationDTO = new DesignationDTO();
				designation.setIsFlag(0);
				designation=dao.save(designation);
				BeanUtils.copyProperties(designation, designationDTO);
				list.add(designationDTO);
				log.info(ConstantExtension.DESIGNATION_DELETED);
				responseModel = new ResponseModelList<>(true, ConstantExtension.DESIGNATION_DELETED, list);
			} else {
				log.info(ConstantExtension.DESIGNATION_NOT_FOUND);
				responseModel = new ResponseModelList<>(true, ConstantExtension.DESIGNATION_NOT_FOUND, list);
			}
		} catch (Exception e) {
			log.error("Error while deleting designation by id");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;

	}

	@Override
	public ResponseModelList<DesignationDTO> findAll() {
		try {
			log.info("finding all designation by id");
			list = new ArrayList<DesignationDTO>();
			List<Designation> designations = dao.findAll();
			if(designations.size()>0)
			for(Designation designation: designations) {
				DesignationDTO designationDTO = new DesignationDTO();
				BeanUtils.copyProperties(designation, designationDTO);
				list.add(designationDTO);
				log.info(ConstantExtension.DESIGNATION_SUCCESS_RECEIVE);
				responseModel = new ResponseModelList<>(true, ConstantExtension.DESIGNATION_SUCCESS_RECEIVE, list);
			} else {
				log.info(ConstantExtension.DESIGNATION_NOT_FOUND);
				responseModel = new ResponseModelList<>(true, ConstantExtension.DESIGNATION_NOT_FOUND, list);
			}
		} catch (Exception e) {
			log.error("Error while finding all designation");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;


	}

	@Override
	public ResponseModelList<DesignationDTO> findAllActive() {
		try {
			log.info("finding all designation by id");
			list = new ArrayList<DesignationDTO>();
			List<Designation> designations = dao.findByIsFlag(1);
			if(designations.size()>0)
			for(Designation designation: designations) {
				DesignationDTO designationDTO = new DesignationDTO();
				BeanUtils.copyProperties(designation, designationDTO);
				list.add(designationDTO);
				log.info(ConstantExtension.DESIGNATION_SUCCESS_RECEIVE);
				responseModel = new ResponseModelList<>(true, ConstantExtension.DESIGNATION_SUCCESS_RECEIVE, list);
			} else {
				log.info(ConstantExtension.DESIGNATION_NOT_FOUND);
				responseModel = new ResponseModelList<>(true, ConstantExtension.DESIGNATION_NOT_FOUND, list);
			}
		} catch (Exception e) {
			log.error("Error while finding all designation");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;

	}
	@Override
	public ResponseModelList<DesignationDTO> deleteAll() {
		try {
			log.info("deleting all designation");
			list = new ArrayList<DesignationDTO>();
			List<Designation> designations = dao.findAll();
			if(designations.size()>0)
			for(Designation designation: designations) {
				DesignationDTO designationDTO = new DesignationDTO();
				designation.setIsFlag(0);
				designation=dao.save(designation);
				BeanUtils.copyProperties(designation, designationDTO);
				list.add(designationDTO);
				log.info(ConstantExtension.DESIGNATION_DELETED);
				responseModel = new ResponseModelList<>(true, ConstantExtension.DESIGNATION_DELETED, list);
			} else {
				log.info(ConstantExtension.DESIGNATION_NOT_FOUND);
				responseModel = new ResponseModelList<>(true, ConstantExtension.DESIGNATION_NOT_FOUND, list);
			}
		} catch (Exception e) {
			log.error("Error while deleting all designation");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;


	}

	private void putValueInResponseModel(boolean status, String message, DesignationDTO dto) {
		this.status = status;
		this.message = message;
		Date dateTime = helperExtension.getDateTime();
		if (!helperExtension.isNullOrEmpty(dto)) {
			Designation model = new Designation();
			BeanUtils.copyProperties(dto, model);
			if (!helperExtension.isNullOrEmpty(dto.getDesignationId())) {
				model.setDesignationId(dto.getDesignationId());
				model.setUpdatedOn(dateTime);
			} else {
				model.setDesignationId("des" + helperExtension.getUniqueId());
				model.setCreatedOn(dateTime);
				model.setUpdatedOn(dateTime);
			}
			model = dao.save(model);
			BeanUtils.copyProperties(model, dto);
			list.add(dto);
		}
	}

}
