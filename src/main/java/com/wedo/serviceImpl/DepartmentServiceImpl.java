package com.wedo.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dao.DepartmentDAO;
import com.wedo.dto.DepartmentDTO;
import com.wedo.helpers.ConstantExtension;
import com.wedo.helpers.HelperExtension;
import com.wedo.model.Department;
import com.wedo.service.DepartmentService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DepartmentServiceImpl implements DepartmentService {

	@Autowired
	DepartmentDAO departmentDAO;

	ResponseModelList<DepartmentDTO> responseModel = new ResponseModelList<>();
	HelperExtension helperExtension = new HelperExtension();

	private boolean status = false;
	private String message = "";
	private List<DepartmentDTO> list = null;

	@Override
	public ResponseModelList<DepartmentDTO> save(DepartmentDTO departmentDTO) {

		try {
			list = new ArrayList<>();
			log.info("Saving Department information");

			if (!helperExtension.isNullOrEmpty(departmentDTO.getDepartmentId())) {
				putValueInResponseModel(true, ConstantExtension.DEPARTMENT_UPDATED, departmentDTO);
			} else {
				putValueInResponseModel(true, ConstantExtension.DEPARTMENT_ADDED, departmentDTO);
			}
			log.info(message);
			responseModel = new ResponseModelList<>(status, message, list);
		} catch (Exception e) {
			log.error("error while saving department information");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;

	}

	@Override
	public ResponseModelList<DepartmentDTO> findById(String designationId) {
		try {
			log.info("Finding Department By Id");
			list = new ArrayList<DepartmentDTO>();
			Department department = departmentDAO.findById(designationId).get();
			if (department != null) {
				DepartmentDTO departmentDTO = new DepartmentDTO();
				BeanUtils.copyProperties(department, departmentDTO);
				list.add(departmentDTO);
				responseModel = new ResponseModelList<>(true, ConstantExtension.DEPARTMENT_SUCCESS_RECEIVE, list);
			} else {
				responseModel = new ResponseModelList<>(true, ConstantExtension.DEPARTMENT_NOT_FOUND, list);
			}
		} catch (Exception e) {
			log.error("Error while finding department by id");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;
	}

	@Override
	public ResponseModelList<DepartmentDTO> deleteById(String designationId) {
		try {
			log.info("Deleting department by id");
			list = new ArrayList<DepartmentDTO>();
			Department department = departmentDAO.findById(designationId).get();
			if (department != null) {
				DepartmentDTO departmentDTO = new DepartmentDTO();
				BeanUtils.copyProperties(department, departmentDTO);
				list.add(departmentDTO);
				responseModel = new ResponseModelList<>(true, ConstantExtension.DEPARTMENT_DELETED, list);
			} else {
				responseModel = new ResponseModelList<>(true, ConstantExtension.DEPARTMENT_NOT_FOUND, list);
			}
		} catch (Exception e) {
			log.error("Error while deleting department by id");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	@Override
	public ResponseModelList<DepartmentDTO> findAll() {
		try {
			log.info("Finding all department");
			list = new ArrayList<DepartmentDTO>();
			List<Department> departments = departmentDAO.findAll();
			if (departments.size() > 0) {
				for (Department department : departments) {
					DepartmentDTO departmentDTO = new DepartmentDTO();
					BeanUtils.copyProperties(department, departmentDTO);
					list.add(departmentDTO);
				}
				responseModel = new ResponseModelList<>(true, ConstantExtension.DEPARTMENT_SUCCESS_RECEIVE, list);
			} else {
				responseModel = new ResponseModelList<>(true, ConstantExtension.DEPARTMENT_NOT_FOUND, list);
			}

		} catch (Exception e) {
			log.error("Error while finding all department");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	@Override
	public ResponseModelList<DepartmentDTO> deleteAll() {
		try {
			log.info("Deleting all department");
			list = new ArrayList<DepartmentDTO>();
			List<Department> departments = departmentDAO.findAll();
			if (departments.size() > 0) {
				for (Department department : departments) {
					DepartmentDTO departmentDTO = new DepartmentDTO();
					department.setIsFlag(0);
					department = departmentDAO.save(department);
					BeanUtils.copyProperties(department, departmentDTO);
					list.add(departmentDTO);
				}
				responseModel = new ResponseModelList<>(true, ConstantExtension.DEPARTMENT_SUCCESS_RECEIVE, list);
			} else {
				responseModel = new ResponseModelList<>(true, ConstantExtension.DEPARTMENT_NOT_FOUND, list);
			}

		} catch (Exception e) {
			log.error("Error while deleting all department");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	private void putValueInResponseModel(boolean status, String message, DepartmentDTO dto) {
		this.status = status;
		this.message = message;
		Date dateTime = helperExtension.getDateTime();
		if (!helperExtension.isNullOrEmpty(dto)) {
			Department model = new Department();
			BeanUtils.copyProperties(dto, model);
			if (!helperExtension.isNullOrEmpty(dto.getDepartmentId())) {
				model.setDepartmentId(dto.getDepartmentId());
				model.setUpdatedOn(dateTime);
			} else {
				model.setDepartmentId("dep" + helperExtension.getUniqueId());
				model.setCreatedOn(dateTime);
				model.setUpdatedOn(dateTime);
			}
			model = departmentDAO.save(model);
			BeanUtils.copyProperties(model, dto);
			list.add(dto);
		}
	}
}
