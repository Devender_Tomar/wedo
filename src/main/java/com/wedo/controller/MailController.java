package com.wedo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.MailingDTO;
import com.wedo.service.MailService;

@RestController
@RequestMapping("mail")
public class MailController{

	@Autowired
	MailService mailService;
	ResponseModelList<MailingDTO> responseModelList= new ResponseModelList<>();

	@PostMapping(value = "send")
	public ResponseModelList<MailingDTO> save(@RequestParam(value = "file", required = false)final List<MultipartFile> file, final String mailingDTO) {
		try {

			MailingDTO mailing= new ObjectMapper().readValue(mailingDTO, MailingDTO.class);
			responseModelList=mailService.save(file, mailing);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseModelList;
	}


	@GetMapping("findAll")
	public ResponseModelList<MailingDTO> findAll() {
		mailService.findAll();
		return null;
	}


	public ResponseModelList<MailingDTO> findById(String mailId) {

		return null;
	}


	public ResponseModelList<MailingDTO> deleteById(String mailId) {

		return null;
	}


	public ResponseModelList<MailingDTO> deleteAll() {
		// TODO Auto-generated method stub
		return null;
	}



}
