package com.wedo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.DepartmentDTO;
import com.wedo.service.DepartmentService;

@RestController
@RequestMapping("department")
public class DepartmentController {

	@Autowired
	DepartmentService departmentService;

	ResponseModelList<DepartmentDTO> responseModel = new ResponseModelList<>();

	@PostMapping("save")
	public ResponseModelList<DepartmentDTO> save(@RequestBody DepartmentDTO DepartmentDTO) {
		try {
			responseModel = departmentService.save(DepartmentDTO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseModel;
	}

	@GetMapping("findById/{designationId}")
	public ResponseModelList<DepartmentDTO> findById(@PathVariable("designationId") String designationId) {
		try {
			responseModel = departmentService.findById(designationId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseModel;
	}

	@DeleteMapping("deleteById/{designationId}")
	public ResponseModelList<DepartmentDTO> deleteById(@PathVariable("designationId") String designationId) {
		try {
			responseModel = departmentService.deleteById(designationId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseModel;
	}

	@GetMapping("findAll")
	public ResponseModelList<DepartmentDTO> findAll() {
		try {
			responseModel = departmentService.deleteAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseModel;
	}

	@DeleteMapping("deleteAll")
	public ResponseModelList<DepartmentDTO> deleteAll() {
		try {
			responseModel = departmentService.deleteAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseModel;
	}

}
