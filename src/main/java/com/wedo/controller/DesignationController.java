package com.wedo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.DesignationDTO;
import com.wedo.service.DesignationService;

@RestController
@RequestMapping("designation")
public class DesignationController {

	@Autowired
	DesignationService designationService;

	ResponseModelList<DesignationDTO> responseModel = new ResponseModelList<>();

	@PostMapping("save")
	public ResponseModelList<DesignationDTO> save(@RequestBody DesignationDTO designationDTO) {

		try {
			responseModel = designationService.save(designationDTO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseModel;
	}

	@GetMapping("findById/{designationId}")
	public ResponseModelList<DesignationDTO> findById(@PathVariable("designationId") String designationId) {

		try {
			responseModel = designationService.findById(designationId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseModel;
	}

	@DeleteMapping("deleteById/{designationId}")
	public ResponseModelList<DesignationDTO> deleteById(@PathVariable("designationId") String designationId) {
		try {
			responseModel = designationService.deleteById(designationId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseModel;
	}

	@GetMapping("findAll")
	public ResponseModelList<DesignationDTO> findAll() {
		try {
			responseModel = designationService.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseModel;
	}

	@DeleteMapping("deleteAll")
	public ResponseModelList<DesignationDTO> deleteAll() {
		try {
			responseModel = designationService.deleteAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseModel;
	}


	@GetMapping("findAllActive")
	public ResponseModelList<DesignationDTO> findAllActive() {
		try {
			responseModel = designationService.findAllActive();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseModel;
	}

}
