package com.wedo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.CompanyDTO;
import com.wedo.service.CompanyService;

@RestController
@RequestMapping("company")
public class CompanyController {

	@Autowired
	CompanyService companyService;

	ResponseModelList<CompanyDTO> responseModel= new ResponseModelList<>();

	@PostMapping(value = "save", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseModelList<CompanyDTO> save(@RequestBody CompanyDTO companyDTO) {
			try {
				responseModel=companyService.save(companyDTO);
			} catch (Exception e) {
				e.printStackTrace();
			}
		return responseModel;
	}



	@GetMapping("findById/{companyId}")
	public ResponseModelList<CompanyDTO> findById(@PathVariable String companyId) {
		try {
			responseModel=companyService.findById(companyId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseModel;
	}


	@DeleteMapping("deleteById/{companyId}")
	public ResponseModelList<CompanyDTO> deleteById(@PathVariable String companyId) {
		try {
			responseModel=companyService.deleteById(companyId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseModel;
	}


	@GetMapping("findAll")
	public ResponseModelList<CompanyDTO> findAll() {

		try {
			responseModel=companyService.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseModel;
	}


}
