package com.wedo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.ClientDTO;
import com.wedo.service.ClientService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("client")
public class ClientController {

	@Autowired
	ClientService clientService;

	ResponseModelList<ClientDTO> responseModelList = new ResponseModelList<>();

	@RequestMapping(method = RequestMethod.POST, value = "save", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseModelList<ClientDTO> save(@RequestBody ClientDTO clientDTO) {
		try {
			responseModelList = clientService.save(clientDTO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseModelList;
	}

	@GetMapping("findById/{clientId}")
	public ResponseModelList<ClientDTO> findById(@PathVariable("clientId") String clientId) {

		try {
			responseModelList = clientService.findById(clientId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseModelList;
	}

	@DeleteMapping("deleteById/{clientId}")
	public ResponseModelList<ClientDTO> deleteById(@PathVariable("clientId") String clientId) {

		try {
			responseModelList = clientService.deleteById(clientId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseModelList;
	}

	@GetMapping("findAll")
	public ResponseModelList<ClientDTO> findAll() {

		try {
			responseModelList = clientService.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseModelList;
	}

	@GetMapping("findAllActive")
	public ResponseModelList<ClientDTO> findAllActive() {

		try {
			responseModelList = clientService.findAllActive();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseModelList;
	}


	@DeleteMapping("deleteAll")
	public ResponseModelList<ClientDTO> deleteAll() {

		try {
			responseModelList = clientService.deleteAll();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseModelList;
	}

	@GetMapping("findByContact/{contactNo}")
	ResponseModelList<ClientDTO> findByContact(@PathVariable String contactNo) {
		try {
			responseModelList = clientService.findByContact(contactNo);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseModelList;

	}

	@GetMapping("findByAadharNO/{aadharNo}")
	ResponseModelList<ClientDTO> findByAadharNO(@PathVariable String aadharNo) {
		try {
			responseModelList = clientService.findByAadharNO(aadharNo);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseModelList;

	}

	@GetMapping("findByEmail/{email}")
	ResponseModelList<ClientDTO> findByEmail(@PathVariable String email) {
		try {
			responseModelList = clientService.findByEmail(email);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseModelList;
	}

	@GetMapping("findByName/{name}")
	ResponseModelList<ClientDTO> findByName(@PathVariable String name) {
		try {
			responseModelList = clientService.findByName(name);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseModelList;
	}
}
