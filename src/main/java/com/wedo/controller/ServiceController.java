package com.wedo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.ServiceDTO;
import com.wedo.service.ServiceService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(value="service")
public class ServiceController {

	@Autowired
	ServiceService serviceService;
	ResponseModelList<ServiceDTO> responseModelList = new ResponseModelList<>();

	@RequestMapping(method = RequestMethod.POST, value = "save", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseModelList<ServiceDTO> save(@RequestBody ServiceDTO serviceDTO) {
		try {

			responseModelList = serviceService.save(serviceDTO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseModelList;
	}


	@GetMapping("findById/{serviceId}")
	public ResponseModelList<ServiceDTO> findById(@PathVariable("serviceId") String serviceId) {
		try {
			log.info("Finding Service By Id");
			responseModelList = serviceService.findById(serviceId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseModelList;
	}


	@DeleteMapping("deleteById/{serviceId}")
	public ResponseModelList<ServiceDTO> deleteById(@PathVariable("serviceId") String serviceId) {

		try {
			responseModelList = serviceService.deleteById(serviceId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseModelList;
	}

	@DeleteMapping("deleteAll")
	public ResponseModelList<ServiceDTO> deleteAll() {
		try {

			responseModelList = serviceService.deleteAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseModelList;
	}

	@GetMapping("findAll")
	public ResponseModelList<ServiceDTO> findAll() {
		try {
			responseModelList = serviceService.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseModelList;
	}
}
