package com.wedo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.FeedbackDTO;
import com.wedo.service.FeedbackService;

@RestController
@RequestMapping("feedback")
public class FeedbackController {
	
	@Autowired
	FeedbackService feedbackService;
	
	ResponseModelList<FeedbackDTO> responseModelList= new ResponseModelList<>();
	
	ResponseModelList<FeedbackDTO> save(FeedbackDTO feedbackDTO){
		try
		{
			responseModelList=feedbackService.save(feedbackDTO);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return responseModelList;
		
	}
	ResponseModelList<FeedbackDTO> findById(String feedbackId){
		try
		{
			responseModelList=feedbackService.findById(feedbackId);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return responseModelList;
	}
	ResponseModelList<FeedbackDTO> findAll()
	{
		try
		{
			responseModelList=feedbackService.findAll();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return responseModelList;
		
	}
	ResponseModelList<FeedbackDTO> deleteById(String feedbackId){
		
		try
		{
			responseModelList=feedbackService.deleteById(feedbackId);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return responseModelList;
	}
	ResponseModelList<FeedbackDTO> deleteAll(){
		
		try
		{
			responseModelList=feedbackService.deleteAll();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return responseModelList;
	}
	ResponseModelList<FeedbackDTO> findByIdClientId(String clientId)
	{
		try
		{
			responseModelList=feedbackService.findByIdClientId(clientId);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return responseModelList;
	}
	
}
