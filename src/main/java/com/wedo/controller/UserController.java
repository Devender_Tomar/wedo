package com.wedo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.UserDTO;
import com.wedo.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(value = "user")
public class UserController {

	@Autowired
	private UserService userService;


	@RequestMapping(value = "save", method = RequestMethod.POST,  produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseModelList<UserDTO> save(@RequestBody UserDTO userDTO, HttpServletRequest httpServletRequest) {
		ResponseModelList<UserDTO> responseModelList = new ResponseModelList<>();
		try {
			log.info("Saving User Information");
			responseModelList = userService.save(userDTO);
		} catch (Exception e) {
			log.info("Error while saving User Information" + getClass().getName());
			e.printStackTrace();
		}
		return responseModelList;

	}

	@GetMapping("findById/{userId}")
	public ResponseModelList<UserDTO> findById(@PathVariable("userId") String userId) {
		ResponseModelList<UserDTO> responseModelList = new ResponseModelList<>();
		try {
			log.info("Finding User Information by id");
			responseModelList = userService.findById(userId);
		} catch (Exception e) {
			log.info("Error while fetching User Information" + getClass().getName());
			e.printStackTrace();
		}
		return responseModelList;
	}

	@DeleteMapping("deleteById/{userId}")
	public ResponseModelList<UserDTO> deleteById(@PathVariable("userId") String userId) {
		ResponseModelList<UserDTO> responseModelList = new ResponseModelList<>();
		try {
			log.info("deleting User Information ########  ");
			responseModelList = userService.deleteById(userId);
		} catch (Exception e) {
			log.info("Error while deleting User Information" + getClass().getName());
			e.printStackTrace();
		}
		return responseModelList;
	}

	@GetMapping("findAll")
	ResponseModelList<UserDTO> findAll() {
		ResponseModelList<UserDTO> responseModelList = new ResponseModelList<>();
		try {
			log.info("fetching User Information");
			responseModelList = userService.findAllActive();
		} catch (Exception e) {
			log.info("Error while saving User Information   ###  " + getClass().getName());
			e.printStackTrace();
		}
		return responseModelList;
	}

	@DeleteMapping("deleteAll")
	public ResponseModelList<UserDTO> deleteAll() {
		ResponseModelList<UserDTO> responseModelList = new ResponseModelList<>();
		try {
			log.info("deleting User Information");
			responseModelList = userService.deleteAll();
		} catch (Exception e) {
			log.info("Error while saving User Information  ###  " + getClass().getName());
			e.printStackTrace();
		}
		return responseModelList;
	}

	@GetMapping("findByUserName")
	public ResponseModelList<UserDTO> findByUserName(@RequestParam String username) {
		ResponseModelList<UserDTO> responseModelList = new ResponseModelList<>();
		try {
			log.info("fetching User Information by Username");
			responseModelList = userService.findByUserName(username);
		} catch (Exception e) {
			log.info("Error while fetching User Information by Username  ###  " + getClass().getName());
			e.printStackTrace();
		}
		return responseModelList;
	}

	@GetMapping("findByEmail/{email}")
	public ResponseModelList<UserDTO> findByEmail(@PathVariable("email") String email) {
		ResponseModelList<UserDTO> responseModelList = new ResponseModelList<>();
		try {
			log.info("fetching User Information");
			responseModelList = userService.findByEmail(email);
		} catch (Exception e) {
			log.info("Error while fetching User Information by email   ###  " + getClass().getName());
			e.printStackTrace();
		}
		return responseModelList;
	}

}
