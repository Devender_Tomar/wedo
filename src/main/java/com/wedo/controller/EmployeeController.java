package com.wedo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.EmployeeDTO;
import com.wedo.service.EmployeeService;

@RestController
@RequestMapping("employee")
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;

	public ResponseModelList<EmployeeDTO> responseModelList = new ResponseModelList<>();

	@RequestMapping(value = "save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseModelList<EmployeeDTO> save(@RequestBody EmployeeDTO employeeDTO) {
		try {
			responseModelList=employeeService.save(employeeDTO);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return responseModelList;
	}

	@GetMapping("findById/{employeeId}")
	public ResponseModelList<EmployeeDTO> findById(@PathVariable("employeeId") String employeeId) {
		try {
			responseModelList=employeeService.findById(employeeId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseModelList;
	}

	@DeleteMapping("deleteById/{employeeId}")
	public ResponseModelList<EmployeeDTO> deleteById(@PathVariable("employeeId") String employeeId) {
		try {
			responseModelList=employeeService.deleteById(employeeId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseModelList;
	}

	@GetMapping("findByEmail/{email}")
	public ResponseModelList<EmployeeDTO> findByEmail(@PathVariable("email") String email) {
		try {
			responseModelList=employeeService.findByEmail(email);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseModelList;
	}

	@GetMapping("findByContact/{contact}")
	public ResponseModelList<EmployeeDTO> findByContact(@PathVariable("contact") String contact) {
		try {
			responseModelList=employeeService.findByContact(contact);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseModelList;
	}

	@GetMapping("findByAadhar/{aadhar}")
	public ResponseModelList<EmployeeDTO> findByAadhar(@PathVariable("aadhar") String aadhar) {
		try {
			responseModelList=employeeService.findByAadhar(aadhar);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseModelList;
	}

	@GetMapping("findByPanCard/{pancardno}")
	public ResponseModelList<EmployeeDTO> findByPanCard(@PathVariable("pancardno") String pancardno) {
		try {
			responseModelList=employeeService.findByPanCard(pancardno);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseModelList;
	}

	@GetMapping("findByDepartment/{departmentId}")
	public ResponseModelList<EmployeeDTO> findByDepartment(@PathVariable("departmentId") String departmentId) {
		try {
			responseModelList=employeeService.findByDepartment(departmentId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseModelList;
	}

	@GetMapping("findByDesignation/{designationId}")
	public ResponseModelList<EmployeeDTO> findByDesignation(@PathVariable("designationId") String designationId) {
		try {
			responseModelList=employeeService.findByDesignation(designationId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseModelList;
	}

	@GetMapping("findByUserType/{userType}")
	public ResponseModelList<EmployeeDTO> findByUserType(@PathVariable("userType") String userType) {
		try {
			responseModelList=employeeService.findByUserType(userType);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseModelList;
	}

	@GetMapping("findAll")
	public ResponseModelList<EmployeeDTO> findAll() {
		try {
			responseModelList=employeeService.findAll();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseModelList;
	}

	@DeleteMapping("deleteAll")
	public ResponseModelList<EmployeeDTO> deleteAll() {
		try {
			responseModelList=employeeService.deleteAll();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseModelList;
	}

}
