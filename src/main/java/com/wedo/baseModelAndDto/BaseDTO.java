package com.wedo.baseModelAndDto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseDTO {

	private Date createdOn;
	private Date updatedOn;

}
