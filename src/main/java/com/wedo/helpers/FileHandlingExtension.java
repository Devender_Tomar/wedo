package com.wedo.helpers;

import java.io.File;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

public class FileHandlingExtension {

	public boolean zip(String inputFolder, String outputFolder) {
		try {
			FileOutputStream fileOutputStream = null;
			fileOutputStream = new FileOutputStream(outputFolder);
			ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream);
			File inputFile = new File(inputFolder);
			if (inputFile.isFile())
				zipFile(inputFile, "", zipOutputStream);
			else if (inputFile.isDirectory())
				zipFolder(zipOutputStream, inputFile, "");
			zipOutputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static void zipFolder(ZipOutputStream zipOutputStream, File inputFolder, String parentName)
			throws IOException {

		String myname = parentName + inputFolder.getName() + "\\";
		ZipEntry folderZipEntry = new ZipEntry(myname);
		zipOutputStream.putNextEntry(folderZipEntry);
		File[] contents = inputFolder.listFiles();
		for (File f : contents) {
			if (f.isFile())
				zipFile(f, myname, zipOutputStream);
			else if (f.isDirectory())
				zipFolder(zipOutputStream, f, myname);
		}
		zipOutputStream.closeEntry();
	}

	@SuppressWarnings("resource")
	public static void zipFile(File inputFile, String parentName, ZipOutputStream zipOutputStream) throws IOException {
		// A ZipEntry represents a file entry in the zip archive
		// We name the ZipEntry after the original file's name
		ZipEntry zipEntry = new ZipEntry(parentName + inputFile.getName());
		zipOutputStream.putNextEntry(zipEntry);

		FileInputStream fileInputStream = new FileInputStream(inputFile);
		byte[] buf = new byte[1024];
		int bytesRead;

		// Read the input file by chucks of 1024 bytes
		// and write the read bytes to the zip stream
		while ((bytesRead = fileInputStream.read(buf)) > 0) {
			zipOutputStream.write(buf, 0, bytesRead);
		}
		// close ZipEntry to store the stream to the file
		zipOutputStream.closeEntry();
	}

	public boolean downloadFile(HttpServletRequest request, HttpServletResponse response, String filePath,
			ServletContext context) {
		try {

			File file = new File(filePath);
			FileInputStream inStream = new FileInputStream(file);
			String extension = StringUtils.substringAfterLast(filePath, ".");
			// gets MIME type of the file
			String mimeType = context.getMimeType("." + extension);
			if (mimeType == null) {
				// set to binary type if MIME mapping not found
				mimeType = "application/octet-stream";
			}

			// modifies response
			response.setContentType(mimeType);
			response.setContentLength((int) file.length());

			// forces download
			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", file.getName());
			response.setHeader(headerKey, headerValue);

			// obtains response's output stream
			OutputStream outStream = response.getOutputStream();
			byte[] buffer = new byte[4096];
			int bytesRead = -1;
			while ((bytesRead = inStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}
			inStream.close();
			outStream.close();

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public void SaveFile(byte[] bytes, String folderPath, String fileName, String fileExt) throws IOException {
		FileOutputStream fop = null;
		HelperExtension.Print(folderPath + fileName);
		try {
			File folder = new File(folderPath);
			if (!folder.exists()) {
				folder.mkdirs();
			}
			File file = null;
			folder.mkdir();
			file = new File(folder, fileName + "." + fileExt);
			file.createNewFile();
			fop = new FileOutputStream(file);
			fop.write(bytes);
		} catch (Exception exception) {
			exception.printStackTrace();
		} finally {
			fop.flush();
			fop.close();
		}
	}

	public void deleteFile(File file, long threadTime) {
		FileDeleteExtension fileDeleteExtension = new FileDeleteExtension(file, threadTime);
		Thread t1 = new Thread(fileDeleteExtension);
		t1.start();
	}

	public void saveFile(byte[] bytes, String folderPath, String fileName) throws IOException {
		FileOutputStream fop = null;
		HelperExtension.Print(folderPath + fileName);
		try {
			File folder = new File(folderPath);
			if (!folder.exists()) {
				folder.mkdirs();
			}
			File file = null;
			folder.mkdir();
			file = new File(folder, fileName);
			file.createNewFile();
			fop = new FileOutputStream(file);
			fop.write(bytes);
		} catch (Exception exception) {
			exception.printStackTrace();
		} finally {
			fop.flush();
			fop.close();
		}
	}

	public static void copyFileUsingStream(File source, File dest) throws IOException {
		InputStream is = null;
		OutputStream os = null;
		try {
			is = new FileInputStream(source);
			os = new FileOutputStream(dest);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
		} finally {
			is.close();
			os.close();
		}
	}

}
