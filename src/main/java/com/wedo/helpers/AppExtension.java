package com.wedo.helpers;

import java.io.File;
import java.net.URLDecoder;
import java.nio.file.FileSystems;

public class AppExtension {

	public String getFolderPath() {
		String baseUrl = "";
		try {
			String path = this.getClass().getClassLoader().getResource("").getPath();
			String fullPath = URLDecoder.decode(path, "UTF-8");
			if (fullPath.contains("WEB-INF")) {
				/* Live Enviornment (Tomcat) */
				String pathArr[] = fullPath.split("/WEB-INF/classes/");
				fullPath = pathArr[0];
				// to read a file from webcontent
				baseUrl = new File(fullPath).getPath() + File.separatorChar;
			} else {
				/* Local Enviornment (IDE) */
				baseUrl = FileSystems.getDefault().getPath("src", "main", "webapp").toUri().toURL().toString();
				baseUrl = baseUrl.replaceAll("file:/", "");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return baseUrl;
	}
}
