package com.wedo.helpers;

import java.io.File;

import java.io.IOException;
import java.net.URLDecoder;
import java.nio.file.FileSystems;

public class FolderPathExtension {

	public static void test() throws IOException {
		String baseUrl = FileSystems.getDefault().getPath("src", "main", "webapp").toUri().toURL().toString();
		HelperExtension.Print("Path 1->" + baseUrl);

		String path = new File(".").getCanonicalPath();
		HelperExtension.Print("Path 2->" + path);

		Test1 test1 = new Test1();
		HelperExtension.Print("Path 3->" + test1.getPath());
	}
}

class Test1 {
	public String getPath() {
		String reponsePath = "";
		try {
			String path = this.getClass().getClassLoader().getResource("").getPath();
			String fullPath = URLDecoder.decode(path, "UTF-8");
			String splitText = "";
			if(fullPath.contains("WEB-INF")) {
				splitText = "WEB-INF";
			}else {
				splitText = "target";
			}
			String pathArr[] = fullPath.split("/"+splitText+"/classes/");
			 System.out.println(fullPath);
			 System.out.println(pathArr[0]);
			fullPath = pathArr[0];
			
			// to read a file from webcontent
			reponsePath = new File(fullPath).getPath() + File.separatorChar;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reponsePath;
	}
}