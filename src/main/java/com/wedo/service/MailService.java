package com.wedo.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.MailingDTO;

public interface MailService {

	ResponseModelList<MailingDTO> save(List<MultipartFile> file, MailingDTO mailingDTO);
	ResponseModelList<MailingDTO> findAll();
	ResponseModelList<MailingDTO> findById(String mailId);
	ResponseModelList<MailingDTO> deleteById(String mailId);
	ResponseModelList<MailingDTO> deleteAll();
}
