package com.wedo.service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.UserDTO;

public interface UserService {

	ResponseModelList<UserDTO> save(UserDTO userDTO);
	ResponseModelList<UserDTO> findById(String userId);
	ResponseModelList<UserDTO> deleteById(String userId);
	ResponseModelList<UserDTO> findAll(String userId);
	ResponseModelList<UserDTO> deleteAll();
	ResponseModelList<UserDTO> findByUserName(String userId);
	ResponseModelList<UserDTO> findByEmail(String userId);
	ResponseModelList<UserDTO> findAllActive();
	
}
