package com.wedo.service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.ServiceDTO;

public interface ServiceService {

	ResponseModelList<ServiceDTO> save(ServiceDTO serviceDTO);
	ResponseModelList<ServiceDTO> findById(String serviceId);
	ResponseModelList<ServiceDTO> deleteById(String serviceId);
	ResponseModelList<ServiceDTO> deleteAll();
	ResponseModelList<ServiceDTO> findAll();
}
