package com.wedo.service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.EmployeeDTO;

public interface EmployeeService {

	ResponseModelList<EmployeeDTO> save(EmployeeDTO employeeDTO);
	ResponseModelList<EmployeeDTO> findById(String employeeId);
	ResponseModelList<EmployeeDTO> deleteById(String employeeId);
	ResponseModelList<EmployeeDTO> findByEmail(String email);
	ResponseModelList<EmployeeDTO> findByContact(String contact);
	ResponseModelList<EmployeeDTO> findByAadhar(String aadhar);
	ResponseModelList<EmployeeDTO> findByPanCard(String pancardno);
	ResponseModelList<EmployeeDTO> findByDepartment(String department);
	ResponseModelList<EmployeeDTO> findByDesignation(String designation);
	ResponseModelList<EmployeeDTO> findByUserType(String userType);
	ResponseModelList<EmployeeDTO> findAll();
	ResponseModelList<EmployeeDTO> deleteAll();

}
