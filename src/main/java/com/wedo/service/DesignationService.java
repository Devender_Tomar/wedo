package com.wedo.service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.DesignationDTO;


public interface DesignationService {

	ResponseModelList<DesignationDTO> save(DesignationDTO designationDTO);
	ResponseModelList<DesignationDTO> findById(String designationId);
	ResponseModelList<DesignationDTO> deleteById(String designationId);
	ResponseModelList<DesignationDTO> findAll();
	ResponseModelList<DesignationDTO> deleteAll();
	ResponseModelList<DesignationDTO> findAllActive();


}
