package com.wedo.service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.CompanyDTO;

public interface CompanyService {

	ResponseModelList<CompanyDTO> save(CompanyDTO companyDTO);
	ResponseModelList<CompanyDTO> findById(String companyId);
	ResponseModelList<CompanyDTO> deleteById(String companyId);
	ResponseModelList<CompanyDTO> findAll();
}
