package com.wedo.service;

import org.springframework.stereotype.Service;

import com.wedo.baseModelAndDto.ResponseModelList;
import com.wedo.dto.FeedbackDTO;


public interface FeedbackService {

	ResponseModelList<FeedbackDTO> save(FeedbackDTO feedbackDTO);
	ResponseModelList<FeedbackDTO> findById(String feedbackId);
	ResponseModelList<FeedbackDTO> findAll();
	ResponseModelList<FeedbackDTO> deleteById(String feedbackId);
	ResponseModelList<FeedbackDTO> deleteAll();
	ResponseModelList<FeedbackDTO> findByIdClientId(String clientId);
}
