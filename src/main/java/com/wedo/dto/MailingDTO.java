package com.wedo.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MailingDTO {

	private String mailingId;
	private List<MailAttachmentsDTO> attachments= new ArrayList<MailAttachmentsDTO>(0);
	private String message;
	private String subject;
	private List<ClientDTO> clients= new ArrayList<ClientDTO>(0);
	private List<EmployeeDTO> employees= new ArrayList<EmployeeDTO>(0);
	private String status;

}
