package com.wedo.dto;

import java.util.ArrayList;
import java.util.List;

import com.wedo.model.MaintenanceTask;
import com.wedo.model.Project;
public class MaintenanceDTO {

	private String maintenanceId;
	private Project project;
	private String description;
	private List<MaintenanceTask> maintenanceTasks = new ArrayList<MaintenanceTask>(0);
	public String getMaintenanceId() {
		return maintenanceId;
	}
	public void setMaintenanceId(String maintenanceId) {
		this.maintenanceId = maintenanceId;
	}
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<MaintenanceTask> getMaintenanceTasks() {
		return maintenanceTasks;
	}
	public void setMaintenanceTasks(List<MaintenanceTask> maintenanceTasks) {
		this.maintenanceTasks = maintenanceTasks;
	}


}
