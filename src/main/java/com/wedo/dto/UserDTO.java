package com.wedo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.wedo.baseModelAndDto.BaseDTO;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTO extends BaseDTO{

	private String userid;

	private RoleDTO role;

	private String firstname;

	private String lastname;

	private String email;

	private String contactno1;

	private String contactno2;

	private String createdBy;

	private String lastloginTime;

	private String lastloginDate;

	private String username;

	private String password;

	private String comfirmpassword;

	private String recoveryEmail;

}
