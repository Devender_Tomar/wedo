package com.wedo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.wedo.baseModelAndDto.BaseDTO;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeDTO extends BaseDTO {

	private String employeeId;
	private DepartmentDTO department=new DepartmentDTO();
	private DesignationDTO designation=new DesignationDTO();
	private String firstname;
	private String lastname;
	private String email;
	private String contactno1;
	private String contactno2;
	private String aadharno;
	private String pancardno;
	private String createdBy;
	private String lastupdatedBy;
	private String isActive;
	private AddressDTO address=new AddressDTO();
	private String userType;

}
