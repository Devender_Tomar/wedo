package com.wedo.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wedo.baseModelAndDto.BaseDTO;

import lombok.Data;

@Data
public class AddressDTO extends BaseDTO {

	private String addressid;
	private String addressStreet1;
	private String addressStreet2;
	private String city;
	private String pincode;
	private String state;
	private String country;
	@JsonIgnore
	private List<ClientDTO> clients = new ArrayList<ClientDTO>(0);
	@JsonIgnore
	private List<EmployeeDTO> employee = new ArrayList<EmployeeDTO>(0);
}
