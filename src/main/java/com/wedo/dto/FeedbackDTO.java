package com.wedo.dto;

import lombok.Data;

@Data
public class FeedbackDTO {

	private String feedbackId;

	private ClientDTO client;

	private String rating;

	private String review;

}
