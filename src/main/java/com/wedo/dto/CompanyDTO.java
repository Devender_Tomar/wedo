package com.wedo.dto;

import lombok.Data;

@Data
public class CompanyDTO {

	private String companyId;
	private String companyname;
	private String companyemail;
	private String companyemailpassword;
	private String companysms;
	private String companysmsno;
	private String companywebsite;
	private String mailserver;
	private String smsserver;
}
