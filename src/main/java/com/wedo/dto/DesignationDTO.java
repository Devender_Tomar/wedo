package com.wedo.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wedo.baseModelAndDto.BaseDTO;

import lombok.Data;

@Data
public class DesignationDTO extends BaseDTO{

	private String designationId;
	private String designationtype;
	@JsonIgnore
	private List<EmployeeDTO> employees = new ArrayList<EmployeeDTO>(0);
}
