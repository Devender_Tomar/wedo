package com.wedo.dto;

import com.wedo.baseModelAndDto.BaseDTO;

import lombok.Data;

@Data
public class MailAttachmentsDTO extends BaseDTO {

	private String attachmentId;
	private String location;
	private MailingDTO mailing;
}
