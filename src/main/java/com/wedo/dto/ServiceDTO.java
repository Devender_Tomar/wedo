package com.wedo.dto;

import com.wedo.baseModelAndDto.BaseDTO;

import lombok.Data;

@Data
public class ServiceDTO extends BaseDTO{

	private String serviceId;
	private String serviceName;
	private String serviceDescription;
	private String createdBy;
	private String lastUpdatedBy;

}
