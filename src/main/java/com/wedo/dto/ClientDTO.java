package com.wedo.dto;

import java.util.ArrayList;
import java.util.List;

import com.wedo.baseModelAndDto.BaseDTO;

import lombok.Data;

@Data
public class ClientDTO extends BaseDTO {

	private String clientId;
	private AddressDTO address=new AddressDTO();
	private String clientFirstName;
	private String clientLastName;
	private String clientMiddleName;
	private String clientEmailId;
	private String clientAadharCard;
	private String clientPanCard;
	private String clientGstInNo;
	private String clientGender;
	private String clientContactNo1;
	private String clientContactNo2;
	private String clientWhatsappNo;
	private String clientEmergencyNo;
	private String clientCompanyName;
	private String clientCompanyAddress;
	private String clientDesignation;
	private String clientWebsite;
	private String clientType;
	private List<FeedbackDTO> feedbacks = new ArrayList<FeedbackDTO>(0);
	private List<InvoiceDTO> invoices = new ArrayList<InvoiceDTO>(0);
	private List<ProjectDTO> project = new ArrayList<ProjectDTO>(0);

}
