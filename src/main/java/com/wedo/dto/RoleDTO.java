package com.wedo.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.wedo.baseModelAndDto.BaseDTO;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RoleDTO extends BaseDTO{

	private String roleId;
	private String role;
	private String roleType;
	@JsonIgnore
	private List<UserDTO> users = new ArrayList<UserDTO>(0);

}
