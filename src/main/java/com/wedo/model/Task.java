package com.wedo.model;
// Generated Apr 5, 2020 3:24:12 PM by Hibernate Tools 5.2.10.Final

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Task generated by Infix.Digital
 */
@Entity
@Table(name = "task", catalog = "wedo")
public class Task extends BaseModel implements java.io.Serializable {

	private static final long serialVersionUID = -2827350127001363991L;

	@Id
	@Column(name = "taskid", unique = true, nullable = false, length = 20)
	private String taskid;
	@Column(name = "taskName", length = 40)
	private String taskName;
	@Column(name = "instruction", length = 100)
	private String instruction;
	@Column(name = "totalHours", length = 10)
	private String totalHours;
	@Column(name = "projectId", length = 20)
	private String projectId;
	@Column(name = "status", length = 20)
	private String status;
	@Column(name = "taskType", length = 30)
	private String taskType;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "task")
	private List<ProjectAssingment> projectAssingments = new ArrayList<ProjectAssingment>(0);

}
