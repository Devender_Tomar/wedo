package com.wedo.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="mailAttachments")
public class MailAttachments {


	@Id
	@Column(name = "attachmentId", nullable = false, unique = true, length = 22)
	private String attachmentId;

	@Column(name = "location", length = 300)
	private String location;

	@ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
	@JoinColumn(name = "mailingId" )
	private Mailing mailing;

}
