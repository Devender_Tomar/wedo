package com.wedo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wedo.model.Department;

@Repository
public interface DepartmentDAO extends  JpaRepository<Department, String>{

}
