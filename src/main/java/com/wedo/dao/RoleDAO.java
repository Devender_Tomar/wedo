package com.wedo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wedo.model.Role;

@Repository
public interface RoleDAO extends JpaRepository<Role, String>{

	Role findByRoleType(String roleType);
	
}
