package com.wedo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wedo.model.Client;

@Repository
public interface ClientDAO extends JpaRepository<Client, String> {

	Client findByClientAadharCard(String clientAadharCard);

	Client findByClientEmailId(String clientEmailId);

	List<Client> findByClientFirstName(String clientFirstName);

	List<Client> findByIsFlag(int i);

	Client findByClientContactNo1(String clientContactNo1);

	Client findByClientContactNo2(String clientContactNo2);
}
