package com.wedo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wedo.model.Mailing;

@Repository
public interface MailDAO extends JpaRepository<Mailing, String>{

}
