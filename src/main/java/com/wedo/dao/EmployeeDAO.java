package com.wedo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.wedo.model.Employee;

@Repository
public interface EmployeeDAO extends JpaRepository<Employee, String>{

	List<Employee> findByIsFlag(int i);
	List<Employee> findByUserType(String userType);

	@Query(value="Select * from employee e where e.designationId =:designationId", nativeQuery = true)
	List<Employee> findByDesignation(String designationId);

	@Query(value="Select * from employee e where e.departmentId =:departmentId", nativeQuery = true)
	List<Employee> findByDepartment(String departmentId);

	Employee findByPancardno(String pancardno);
	Employee findByAadharno(String aadharno);

	Employee findByContactno1(String contactno1);
	Employee findByContactno2(String contactno2);

	Employee findByEmail(String email);

}
