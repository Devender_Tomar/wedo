package com.wedo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wedo.model.Service;

@Repository
public interface ServiceDAO extends JpaRepository<Service, String> {

	Service findByServiceName(String serviceName);
	
	
}
