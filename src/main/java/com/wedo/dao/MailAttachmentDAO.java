package com.wedo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wedo.model.MailAttachments;

@Repository
public interface MailAttachmentDAO extends JpaRepository<MailAttachments, String>{

}
