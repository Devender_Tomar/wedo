package com.wedo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wedo.model.User;

@Repository
public interface UserDAO extends JpaRepository<User, String>{


	List<User> findByEmail(String email);

	User findByUsername(String username);
	List<User> findByIsFlag(int i);
	List<User> findByContactno1(String contactno1);
	List<User> findByContactno2(String contactno2);


}
