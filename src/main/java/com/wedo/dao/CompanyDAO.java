package com.wedo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wedo.model.Company;

@Repository
public interface CompanyDAO extends JpaRepository<Company, String>{

}
