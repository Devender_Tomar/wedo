
# To set logs level as per your need.
logging.level.org.springframework = error
logging.level.tech.hardik = trace

# To store logs to external file
# Here use strictly forward "/" slash for both Windows, Linux or any other os, otherwise, your logs it won't work.      
logging.file=/src/main/resources/log.txt

# To customize logging pattern.
logging.pattern.file= "%d{yyyy-MM-dd HH:mm:ss} - %msg%n"